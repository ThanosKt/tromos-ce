/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

type DefaultCoordinator struct{}

func (DefaultCoordinator) SetBackend(coordinator.Coordinator) {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) String() string {
	return "DefaultCoordinator"
}

func (DefaultCoordinator) Capabilities() []selector.Capability {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) Location() string {
	return "localhost"
}

func (DefaultCoordinator) Close() error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) Info(key string) ([][]byte, coordinator.Info, error) {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) CreateOrReset(key string) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) CreateIfNotExist(key string) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) SetLandmark(key string, mark coordinator.Landmark) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) UpdateStart(key string, tid string, intentions coordinator.IntentionRecord) error {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) UpdateEnd(key string, tid string, record []byte) error {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) ViewStart(key string, filter []string) (records [][]byte, tids []string, err error) {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) ViewEnd(tids []string) error {

	panic(coordinator.ErrBackend)
}
