/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
)

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

// Synthesizes composes a virtual Coordinator instances by piggybacking layers
// (persistent, translators, proxy)
func Synthesize(cid string, config *viper.Viper) (coordinator.Coordinator, error) {
	if config == nil {
		return nil, coordinator.ErrInvalid
	}

	var layer coordinator.Coordinator
	islocal := net.IsLocalAddress(config.GetString("Proxy.host"))

	// It is local when 1) there is no proxy 2) the proxy ip is a local ip
	if !islocal {
		proxyclient := config.Sub("proxy")
		plugin, err := hub.OpenCoordinatorPlugin(proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}
		layer, err = plugin(proxyclient)
		if err != nil {
			return nil, err
		}

		layer.SetBackend(DefaultCoordinator{})
	} else {
		persistent := config.Sub("persistent")

		plugin, err := hub.OpenCoordinatorPlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer, err = plugin(persistent)
		if err != nil {
			return nil, err
		}

		layer.SetBackend(DefaultCoordinator{})

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.GetStringMap("Translators"))
		for _, seqID := range stack {
			translator := config.Sub("translators." + seqID)

			plugin, err := hub.OpenCoordinatorPlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer, err := plugin(translator)
			if err != nil {
				return nil, err
			}

			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local coordinator exposed through proxy (the proxy ip is the local ip)
		if len(config.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Sub("proxy")
			plugin, err := hub.OpenCoordinatorPlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server, err := plugin(proxyserver)
			if err != nil {
				return nil, err
			}

			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, cap := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[cap]
		if !ok {
			return nil, coordinator.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	log.User("Found Coordinator ", cid, "@", layer.Location())
	// TODO: String to capabilities
	//viper.GetStringSlice(alias + ".Capabilities"),
	return &Coordinator{
		Coordinator:  layer,
		identifier:   cid,
		capabilities: capabilities,
	}, nil
}
