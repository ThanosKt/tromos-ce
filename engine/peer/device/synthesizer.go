/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
)

// Synthesize s analyzes the configuration and decides whether
// to spawn a new Device stack or return a client to a remote Device
func Synthesize(rid string, config *viper.Viper) (device.Device, error) {
	if config == nil {
		return nil, device.ErrInvalid
	}

	var layer device.Device
	islocal := net.IsLocalAddress(config.GetString("Proxy.host"))

	// It is local when 1) there is no proxy 2) the proxy ip is a local ip
	if !islocal {
		proxyclient := config.Sub("proxy")
		plugin, err := hub.OpenDevicePlugin(proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}

		layer = plugin(proxyclient)
		layer.SetBackend(DefaultDevice{})
	} else {
		persistent := config.Sub("persistent")
		plugin, err := hub.OpenDevicePlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer = plugin(persistent)
		layer.SetBackend(DefaultDevice{})

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.GetStringMap("translators"))
		for _, seqID := range stack {
			translator := config.Sub("translators." + seqID)
			plugin, err := hub.OpenDevicePlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer := plugin(translator)
			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local device exposed through proxy (the proxy ip is the local ip)
		if len(config.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Sub("proxy")
			plugin, err := hub.OpenDevicePlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server := plugin(proxyserver)
			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, cap := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[cap]
		if !ok {
			return nil, device.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	log.User("Found Device ", rid, "@", layer.Location())
	return &Device{
		Device:       layer,
		identifier:   rid,
		capabilities: capabilities,
	}, nil
}
