/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"bytes"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	hub "gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"io"
	"testing"
)

var iterations int = 3

var textExample []byte

var yamlExample = []byte(`
--- 
Devices: 
  dev0: 
    Persistent: 
      path: /tmp/scratch/hdd0
      plugin: gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Capabilities: 
      - default
      - scannable
      - cleanstart
  dev1: 
    Persistent: 
      path: /tmp/scratch/hdd1
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Capabilities: 
      - default
      - scannable
      - cleanstart
    Translators: 
      ? "0"
      : 
        capacity: 1B
        plugin:  gitlab.com/tromos/tromos-ce/hub/device/throttler
        rate: 500MB
        regulate: channel
  dev2: 
    Persistent: 
      path: /tmp/scratch/hdd2
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Capabilities: 
      - default
      - scannable
      - cleanstart
    Translators: 
      ? "1"
      : 
        blocksize: 4M
        plugin:  gitlab.com/tromos/tromos-ce/hub/device/blob
  dev3: 
    Persistent: 
      path: /tmp/scratch/hdd3
      plugin: gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Capabilities: 
      - default
      - scannable
      - cleanstart
    Translators: 
      ? "0"
      : 
        capacity: 1B
        plugin:  gitlab.com/tromos/tromos-ce/hub/device/throttler
        rate: 500MB
        regulate: channel
      ? "1"
      : 
        blocksize: 4M
        plugin:  gitlab.com/tromos/tromos-ce/hub/device/blob
  dev4: 
    Persistent: 
      path: /tmp/scratch/hdd4
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Proxy: 
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc
      timeout: 10m
      host: "127.0.0.1"
      port: 7763
    Capabilities: 
      - default
      - scannable
      - cleanstart

  dev5: 
    Persistent: 
      path: /tmp/scratch/hdd5
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/filesystem
      family: os
    Proxy: 
      plugin:  gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc
      timeout: 10m
      host: "127.0.0.1"
      port: 7764
    Capabilities: 
      - default
      - scannable
      - cleanstart
    Translators: 
      ? "0"
      : 
        capacity: 1B
        plugin: gitlab.com/tromos/tromos-ce/hub/device/throttler
        rate: 500MB
        regulate: channel
      ? "1"
      : 
        blocksize: 1M
        plugin: gitlab.com/tromos/tromos-ce/hub/device/blob
`)

var devices []device.Device

func init() {
	viper.SetConfigType("yaml") // or viper.SetConfigType("YAML")
	if err := viper.ReadConfig(bytes.NewBuffer(yamlExample)); err != nil {
		panic(err)
	}

	man, err := manifest.NewManifest(viper.GetViper())
	if err != nil {
		panic(err)
	}

	// Find and download all the plugins for the peer
	pluginList := man.PluginsOnPeer("127.0.0.1")
	if len(pluginList) == 0 {
		panic("No plugins were defined")
	}

	// Download all the necessary plugins
	if err := hub.FixDependencies(pluginList); err != nil {
		panic(err)
	}
}

func TestSynthesize(t *testing.T) {
	var dev device.Device
	var err error

	_, err = Synthesize("", nil)
	assert.Equal(t, err, device.ErrInvalid, "Return error should be empty")

	_, err = Synthesize("", viper.Sub("NonExistent"))
	assert.Equal(t, err, device.ErrInvalid, "Return error should be invalid")

	// Test connector
	dev, err = Synthesize("dev0", viper.Sub("devices.dev0"))
	assert.Nil(t, err)
	devices = append(devices, dev)

	// Test connector + throttle
	dev, err = Synthesize("dev1", viper.Sub("devices.dev1"))
	assert.Nil(t, err)
	devices = append(devices, dev)

	// Test connector + blob
	dev, err = Synthesize("dev2", viper.Sub("devices.dev2"))
	assert.Nil(t, err)
	devices = append(devices, dev)

	// Test connector + throttle + blob
	dev, err = Synthesize("dev3", viper.Sub("devices.dev3"))
	assert.Nil(t, err)
	devices = append(devices, dev)

	// Test connector + proxy
	dev, err = Synthesize("dev4", viper.Sub("devices.dev4"))
	assert.Nil(t, err)
	devices = append(devices, dev)

	// Test connector + translator  throttle+blob + proxy
	dev, err = Synthesize("dev5", viper.Sub("devices.dev5"))
	assert.Nil(t, err)
	devices = append(devices, dev)
}

func TestTransfer(t *testing.T) {
	t.Run(devices[0].String(), testSequentialWrites(devices[0]))
	t.Run(devices[1].String(), testSequentialWrites(devices[1]))
	t.Run(devices[2].String(), testSequentialWrites(devices[2]))
	t.Run(devices[3].String(), testSequentialWrites(devices[3]))
	t.Run(devices[4].String(), testSequentialWrites(devices[4]))
	t.Run(devices[5].String(), testSequentialWrites(devices[5]))

	t.Run(devices[0].String(), testParallelWrites(devices[0]))
	t.Run(devices[1].String(), testParallelWrites(devices[1]))
	t.Run(devices[2].String(), testParallelWrites(devices[2]))
	t.Run(devices[3].String(), testParallelWrites(devices[3]))
	t.Run(devices[4].String(), testParallelWrites(devices[4]))
	t.Run(devices[5].String(), testParallelWrites(devices[5]))
}

func testSequentialWrites(dev device.Device) func(t *testing.T) {
	return func(t *testing.T) {
		err := sequentialWrite(dev, 1)
		assert.Nil(t, err)
	}
}

func sequentialWrite(dev device.Device, factor int) error {
	chw, err := dev.NewWriteChannel(uuid.Once())
	if err != nil {
		return err
	}

	for i := 0; i < iterations*factor; i++ {
		pr, pw := io.Pipe()
		if err := chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})}); err != nil {
			return err
		}

		if _, err = io.Copy(pw, bytes.NewReader(textExample)); err != nil {
			return err
		}

		if _, err := io.Copy(pw, bytes.NewReader(textExample)); err != nil {
			return err
		}

		if err := pw.Close(); err != nil {
			return err
		}
	}
	if err := chw.Close(); err != nil {
		return err
	}

	return nil
}

func testParallelWrites(dev device.Device) func(t *testing.T) {
	return func(t *testing.T) {
		err := parallelWrite(dev, 1)
		assert.Nil(t, err)
	}
}

func parallelWrite(dev device.Device, factor int) error {
	chw, err := dev.NewWriteChannel(uuid.Once())
	if err != nil {
		return err
	}

	for i := 0; i < iterations*factor; i++ {

		pr, pw := io.Pipe()
		if err := chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})}); err != nil {
			return err
		}

		go func() {
			if _, err := io.Copy(pw, bytes.NewReader(textExample)); err != nil {
				panic(err)
			}

			if _, err := io.Copy(pw, bytes.NewReader(textExample)); err != nil {
				panic(err)
			}

			if err := pw.Close(); err != nil {
				panic(err)
			}
		}()
	}

	if err := chw.Close(); err != nil {
		return err
	}
	return nil
}

func BenchmarkSequentialDev0(b *testing.B) {
	err := sequentialWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev1(b *testing.B) {
	err := sequentialWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}

}
func BenchmarkSequentialDev2(b *testing.B) {
	err := sequentialWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev3(b *testing.B) {
	err := sequentialWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev4(b *testing.B) {
	err := sequentialWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev5(b *testing.B) {
	err := sequentialWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev0(b *testing.B) {
	err := parallelWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev1(b *testing.B) {
	err := parallelWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}
}
func BenchmarkParallelDev2(b *testing.B) {
	err := parallelWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev3(b *testing.B) {
	err := parallelWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev4(b *testing.B) {
	err := parallelWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev5(b *testing.B) {
	err := parallelWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}
