/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

type DefaultDevice struct{}

func (DefaultDevice) SetBackend(device.Device) {
	panic(device.ErrBackend)
}

func (DefaultDevice) String() string {
	return "DefaultDevice"
}

func (DefaultDevice) Capabilities() []selector.Capability {
	panic(device.ErrBackend)
}

func (DefaultDevice) Location() string {
	return "localhost"
}

func (DefaultDevice) NewWriteChannel(collectionName string) (device.WriteChannel, error) {
	panic(device.ErrBackend)
}

func (DefaultDevice) NewReadChannel(string) (device.ReadChannel, error) {
	panic(device.ErrBackend)
}

func (DefaultDevice) Scan() ([]string, []device.Item, error) {
	panic(device.ErrBackend)
}
func (DefaultDevice) Close() error {
	panic(device.ErrBackend)
}
