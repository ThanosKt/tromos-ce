/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

// Device inherits the properties of backend device.Device and augments with
// a unique identifier used for device selection on a Mesh of Device
type Device struct {
	device.Device
	identifier   string
	peer         string
	capabilities []selector.Capability
}

func (d *Device) String() string {
	return d.identifier
}

func (d *Device) Location() string {
	return d.peer
}

func (d *Device) Capabilities() []selector.Capability {
	return d.capabilities
}
