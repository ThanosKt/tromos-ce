/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"net/http"
)

// RunWebservice makes the peer functionality availablle through RPC
func (peer *Peer) RunWebservice() error {
	ops := rpc.NewWebSocketService()
	ops.AddFunction("Bootstrap", peer.Bootstrap)

	webservice := &http.Server{
		Addr:    peer.address + ":" + WebServicePort,
		Handler: ops,
	}
	peer.webservice = webservice

	errCh := make(chan error)

	go func() {
		peer.logger.Printf("Starting Webservice at %s", webservice.Addr)
		err := webservice.ListenAndServe()
		errCh <- err
		close(errCh)
	}()
	return <-errCh
}
