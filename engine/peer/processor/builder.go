/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
	"reflect"
)

type Builder struct {
	network *flow.Graph
	// linking stuff
	capabilitymap map[string][]selector.Capability // port -> key -> value

	// Internal network state
	inport     chan *processor.Stream // Input to the process network
	outports   []string               // Port identifiers
	readyports []reflect.SelectCase   // Ports ready to send/receive data
}

func newBuilder(writable bool, graph processor.ProcessGraph) *Builder {

	network := new(flow.Graph)
	network.InitGraphState()

	b := &Builder{
		network:       network,
		capabilitymap: make(map[string][]selector.Capability),
	}

	// populate the blank network with components
	if writable {
		graph.Upstream(b)
	} else {
		graph.Downstream(b)
	}
	// and instantiate it

	b.inport = make(chan *processor.Stream)
	b.network.SetInPort("In", b.inport)

	outportsV := reflect.ValueOf(*b.network).FieldByName("outPorts").MapKeys()
	b.outports = make([]string, len(outportsV))
	for i := 0; i < len(outportsV); i++ {
		b.outports[i] = outportsV[i].String()
	}

	// Create asynchronous notification channels for the network output
	// ports to signal processor sinks when they have data
	b.readyports = make([]reflect.SelectCase, len(b.outports))
	for i := 0; i < len(b.outports); i++ {

		// Set notification channel for ports that have data
		ch := make(chan *processor.Stream)
		b.network.SetOutPort(b.outports[i], ch)
		b.readyports[i] = reflect.SelectCase{
			Dir:  reflect.SelectRecv,
			Chan: reflect.ValueOf(ch),
		}
	}

	// Wait until the net has completed its job
	flow.RunNet(b.network)
	<-b.network.Ready()

	return b
}

func (b *Builder) Close() error {
	//close(b.inport)
	//<-b.network.Wait()
	return nil
}

func (b *Builder) Add(name string, module interface{}) bool {
	return b.network.Add(module, name)
}

func (b *Builder) Connect(senderName, senderPort, receiverName, receiverPort string) bool {
	return b.network.Connect(senderName, senderPort, receiverName, receiverPort)
}

func (b *Builder) MapInPort(port, app, appPort string) {
	if !b.network.MapInPort(port, app, appPort) {
		panic("MapInPort failed")
	}
}

func (b *Builder) MapOutPort(port, app, appPort string, annotations ...selector.Capability) {
	b.network.MapOutPort(port, app, appPort)
	b.capabilitymap[port] = annotations
}

func (b *Builder) Capabilities(port string) []selector.Capability {
	return b.capabilitymap[port]
}

func (b *Builder) NewTransfer(stream *processor.Stream) error {
	b.inport <- stream
	return nil
}

func (b *Builder) Sinks() map[string]string {
	sinks := make(map[string]string)
	for _, port := range b.outports {
		sinks[port] = ""
	}
	return sinks
}

func (b *Builder) Readyports() chan *processor.Stream {
	events := make(chan *processor.Stream)

	go func() {
		for i := 0; i < len(b.readyports); i++ {
			chosen, value, _ := reflect.Select(b.readyports)

			substream := value.Interface().(*processor.Stream)
			// This may happen for modules that on donwlink do not wish to consume
			// data from all the connected sinks (e.g., mirror)
			if substream == nil {
				continue
			}
			_ = chosen
			substream.Port = b.outports[chosen]
			events <- substream
		}
		close(events)
	}()
	return events
}
