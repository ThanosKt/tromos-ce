/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

// Synthesize composes the Processor as specified in the configuration
func Synthesize(pid string, config *viper.Viper) (processor.Processor, error) {
	if config == nil {
		return nil, processor.ErrInvalid
	}

	graphconfig := config.Sub("graph")
	plugin, err := hub.OpenProcessorPlugin(graphconfig.GetString("plugin"))
	if err != nil {
		return nil, err
	}

	graph := plugin(graphconfig)

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, cap := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[cap]
		if !ok {
			return nil, processor.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	//log.User("Found Processor ", pid, "@", layer.Location())
	log.User("Found Processor ", pid, "@localhost")
	p := &Processor{
		identifier:   pid,
		capabilities: capabilities,
	}
	p.init(graph)

	return p, nil
}
