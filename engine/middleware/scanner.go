/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

func (client *Client) ImportFrom(deviceID string) error {
	device, err := client.DeviceManager.GetDevice(deviceID)
	if err != nil {
		return err
	}

	files, items, err := device.Scan()
	if err != nil {
		return err
	}

	_ = files
	_ = items
	/*
		if len(items) == 0 {
			log.User("No items to index for resource:", deviceID)
			return nil
		}

		for i := 0; i < len(files); i++ {
			item := items[i]
			filename := files[i]

			partition := client.Namespace.Partition(filename)
			err := partition.CreateIfNotExist(filename)
			if err != nil {
				return err
			}

			// Use as default sink the "root.scannable"
			tx, err := client.BeginUpdate(filename, "", []processing.Sink{
				processing.Sink{WalkID: "root.scannable", DeviceID: deviceID},
			})
			if err != nil {
				return err
			}

			delta := tx.NewDelta(
				&processing.Stream{
					Items: map[string]data.Item{"root.scannable": item},
				})
			delta.Offset = 0
			delta.Size += int(item.Size)

			if err := client.EndUpdate(tx); err != nil {
				return err
			}
		}
		log.User("Indexed: ", len(items), " items for resource:", deviceID)
		return nil
	*/
	return nil
}
