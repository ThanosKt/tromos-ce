/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrMiddleware = errors.NewClass("Device Error")
	ErrArg        = ErrMiddleware.NewClass("Argument error")
	ErrRuntime    = ErrMiddleware.NewClass("Runtime error")

	// Generic family errors
	ErrBackend           = ErrMiddleware.New("Backend error")
	ErrOnlyLocalDatapath = ErrMiddleware.New("Only local datapath is supported for the momenet")

	// Argument family errors
	ErrInvalid = ErrArg.New("Invalid argument")

	// Runtime family errors
	ErrStaleCluster = ErrRuntime.New("Stale cluster information")
)
