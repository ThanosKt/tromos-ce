/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gopkg.in/go-playground/validator.v9"
)

type ProcessorManagerConfig struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// ProcessorManager embeds cluster knowledge for all the Processor in the container.
// It is a distributed service running on every node that hosts or wants to have access
// to Processor. it is assumed that all distributed instances share the cluster knowledge.
//
// When a client wants to access a Processor, it asks the Processor Manager to return
// the appropriate connector.
type ProcessorManager struct {
	config *ProcessorManagerConfig
	mesh   map[string]processor.Processor
}

func NewProcessorManager(config *ProcessorManagerConfig) (*ProcessorManager, error) {

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	pm := &ProcessorManager{
		config: config,
		mesh:   make(map[string]processor.Processor),
	}

	for pid, proc := range config.Peer.Processors() {
		pm.mesh[pid] = proc
		pm.config.Selector.Add(selector.SelectorProperties{
			ID:           pid,
			Capabilities: proc.Capabilities(),
			Peer:         proc.Location(),
		})
	}
	pm.config.Selector.Commit()

	return pm, nil
}

// Close closes the device manager
func (pm *ProcessorManager) Close() error {
	return nil
}

// SelectAndReserve selects and reserves one of the available Processors in the cluster based
// on the capability contrains. It returns the Processor ID
func (pm *ProcessorManager) SelectAndReserve(capabilities ...selector.Capability) (processor.Processor, error) {
	procid, err := pm.config.Selector.Select(nil, capabilities...)
	if err != nil {
		return nil, err
	}
	return pm.GetProcessor(procid)
}

// GetProcessor returns a connector to the Processor identified by processorID
func (pm *ProcessorManager) GetProcessor(processorID string) (processor.Processor, error) {
	proc, ok := pm.mesh[processorID]
	if !ok {
		return nil, ErrStaleCluster
	}
	return proc, nil
}
