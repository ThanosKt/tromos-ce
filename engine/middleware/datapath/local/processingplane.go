/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/local"

import (
	"github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

type ProcessPlane struct {
	flow.Component
	logger *logrus.Entry

	In  <-chan *datapath.Discovery
	Out chan<- *datapath.Discovery

	processor.Processor
}

func (plane *ProcessPlane) OnIn(req *datapath.Discovery) {
	newReservation(req, plane.Processor, plane.Out)
}

func newReservation(req *datapath.Discovery, processor processor.Processor, outs chan<- *datapath.Discovery) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	ch, err := processor.NewChannelFromConfig(req.ChannelConfig)
	if err != nil {
		datapath.Abort(req, err)
		return
	}
	defer ch.Close()

	// discover and reserve links with the components connected to the Processor's outputs
	nextHops := make(map[string]*datapath.Reservation)
	for port := range ch.Sinks() {

		reservation, err := datapath.Discover(outs, req, port, ch.Capabilities(port)...)
		if err != nil {
			datapath.Abort(req, err)
			return
		}
		nextHops[port] = reservation
	}

	reservation := datapath.Reserve(req)
	defer datapath.Release(reservation)

	for in := range reservation.Listener {
		if err := ch.NewTransfer(in); err != nil {
			datapath.Abort(req, err)
			return
		}
		for substream := range ch.Readyports() {
			// Forward the stream as soon as its ready
			nextHops[substream.Port].Listener <- substream
		}
	}

	//	Terminate processor
	for _, nexthop := range nextHops {
		datapath.WaitRelease(nexthop)
	}
}
