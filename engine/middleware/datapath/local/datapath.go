/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/local"

import (
	"github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gopkg.in/go-playground/validator.v9"
)

// LocalDatapath connect local processor with local device manager
type LocalDatapath struct {
	config   datapath.DatapathConfig
	discover chan *datapath.Discovery
	network  *flow.Graph
}

func NewLocalDatapath(opts ...datapath.DatapathOption) (*LocalDatapath, error) {

	config := datapath.DatapathConfig{}
	for _, opt := range opts {
		opt(&config)
	}

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	proc, err := config.ProcessorManager.SelectAndReserve()
	if err != nil {
		return nil, err
	}

	pplane := &ProcessPlane{
		logger:    logrus.WithField("module", "LocalDatapath/ProcessPlane"),
		Processor: proc}
	splane := &StoragePlane{
		logger: logrus.WithField("module", "LocalDatapath/StoragePlane"),
	}

	// Init IOdiscover network (the custom graph that will be used)
	network := new(flow.Graph)
	network.InitGraphState()

	network.Add(pplane, "InTransit")
	network.Add(splane, "Storage")
	network.MapInPort("In", "InTransit", "In")
	network.Connect("InTransit", "Out", "Storage", "In")

	discover := make(chan *datapath.Discovery)
	network.SetInPort("In", discover)

	flow.RunNet(network)
	<-network.Ready()

	return &LocalDatapath{
		config:   config,
		discover: discover,
		network:  network,
	}, nil
}

func (_ *LocalDatapath) String() string {
	return "LocalDatapath"
}

func (_ *LocalDatapath) Reusable() bool {
	return false
}

func (_ *LocalDatapath) Capabilities() []selector.Capability {
	return nil
}

func (_ *LocalDatapath) Location() string {
	return ""
}

func (path *LocalDatapath) Close() error {
	// FIXME: Should I use it ?
	close(path.discover)
	<-path.network.Wait()
	return nil
}

func (path *LocalDatapath) NewChannel(opts ...processor.ChannelOption) (processor.Channel, error) {

	var config processor.ChannelConfig
	for _, opt := range opts {
		opt(&config)
	}
	return path.NewChannelFromConfig(config)
}

func (path *LocalDatapath) NewChannelFromConfig(config processor.ChannelConfig) (processor.Channel, error) {

	// TODO: if ever migrate to distributedDatapath, do not forget to set the
	// Authority Device Manager to the local device manager of the client
	// who initiates the channel (or to the central Authority Device Manager, if any)
	// To do so, use the cluster-facing ip into the WalkID
	request := &datapath.Discovery{
		DeviceManager: path.config.DeviceManager,
		ChannelConfig: config,
	}

	reservation, err := datapath.Discover(path.discover, request, "127.0.0.1")
	if err != nil {
		return nil, err
	}

	return &channel{
		request:  request,
		response: reservation,
	}, nil

}

type channel struct {
	request  *datapath.Discovery
	response *datapath.Reservation
}

func (ch *channel) NewTransfer(stream *processor.Stream) error {
	ch.response.Listener <- stream
	return nil
}

func (ch *channel) Close() error {
	datapath.WaitRelease(ch.response)
	return nil
}

func (ch *channel) Capabilities(port string) []selector.Capability {
	panic("Should not be called")
}

func (ch *channel) Sinks() map[string]string {
	return ch.request.ChannelConfig.Sinks
}

func (ch *channel) Readyports() chan *processor.Stream {
	panic("Should not be called")
}
