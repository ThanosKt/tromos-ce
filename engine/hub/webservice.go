/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"io/ioutil"
	"net/http"
	"os"
)

var (
	Service = ":7777"
)

func (hub *Hub) getPlugin(name string) ([]byte, error) {
	hub.logger.Debugf("Download request %s", name)

	// [1:] is to ommit the first start of the url
	pluginpath := hub.shortenPath(name)

	//Check if file exists and open
	file, err := os.Open(pluginpath)
	defer file.Close() //Close after function return
	if err != nil {
		hub.logger.WithError(err).Warnf("Open error %s", name)
		return nil, err
	}

	defer hub.logger.Infof("Plugin %s succesfully downloaded", pluginpath)

	return ioutil.ReadFile(pluginpath)
}

// RunDistributionService calls RunDistributionService method on the default Hub
func RunDistributionService() {
	hub.RunDistributionService()
}

// RunDistributionService runs a sever in the default hub for distributing
// binary plugins to the daemons.
func (hub *Hub) RunDistributionService() {

	ops := rpc.NewWebSocketService()
	ops.AddFunction("Getplugin", hub.getPlugin)

	webservice := &http.Server{
		Addr:    Service,
		Handler: ops,
	}

	if err := webservice.ListenAndServe(); err != nil {
		hub.logger.WithError(err).Fatal("Webservice error")
	}

}

type WebServiceOperations struct {
	GetPlugin func(name string) ([]byte, error)
}

func (hub *Hub) GetPluginFromDistributionService(name string) error {

	// otherwise start a new instance
	call := &WebServiceOperations{}
	conn := rpc.NewWebSocketClient("ws://" + Service + "/")
	conn.UseService(call)

	pluginBin, err := call.GetPlugin(name)
	if err != nil {
		return err
	}

	//	hub.shortenPath(name)

	return ioutil.WriteFile("/tmp/poutses", pluginBin, 0644)
}
