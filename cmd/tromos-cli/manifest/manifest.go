/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package manifest // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli/manifest"

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
)

func init() {
	ManifestCmd.AddCommand(validateCmd)
	validateCmd.Flags().String("manifest", "", "Location to manifest")
}

var ManifestCmd = &cobra.Command{
	Use:   "manifest",
	Short: "manifest related operations",
	Long:  `Handle all the operations related to a container manifest`,
}

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "validate the manifest",
	RunE: func(cmd *cobra.Command, args []string) error {
		flags := cmd.Flags()
		if err := viper.BindPFlag("manifest", flags.Lookup("manifest")); err != nil {
			return err
		}

		localmanifest := viper.New()

		// Load a local manifest
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		if err := man.Validate(); err != nil {
			return err
		}
		return nil
	},
}
