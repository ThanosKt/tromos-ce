/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package main // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/gateway"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/hub"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/manifest"
	"os"
	"os/signal"
)

func init() {
	// initial log formatting; this setting is updated after the daemon configuration is loaded.
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	RootCmd.AddCommand(gateway.GatewayCmd)
	RootCmd.AddCommand(manifest.ManifestCmd)
	RootCmd.AddCommand(hub.HubCmd)
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "tromos",
	Short:                 "Tromos command line tool",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		if err := RootCmd.Execute(); err != nil {
			errChan <- err
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block waiting either an error to occur to a termination signal to come
	select {
	case <-c:
		signal.Reset(os.Interrupt)
	case err := <-errChan:
		if err != nil {
			logrus.Fatal(err)
		}
	}
}
