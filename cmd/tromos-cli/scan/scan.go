/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package scan // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli/scan"

/*
import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"log"
)

func init() {
	cli.RootCmd.AddCommand(scanCmd)

	flags := startCmd.Flags()
	flags.String("manifest", "", "Execute from manifest")
	if err := scanCmd.MarkFlagRequired("manifest"); err != nil {
		panic(err)
	}

	flags.String("fromdevice", "", "Device to import data")
	if err := scanCmd.MarkFlagRequired("fromdevice"); err != nil {
		panic(err)
	}
}

var scanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Index the contents of existing systems",
	Long:  "Index the contents of existing systems",
	RunE: func(cmd *cobra.Command, args []string) error {

		localmanifest := viper.New()
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		client, err := middleware.NewClient(middleware.ClientConfig{
			Manifest: man,
		})
		if err != nil {
			return err
		}
		defer client.Close()

		deviceid, err := cmd.Flags().GetString("fromdevice")
		if err != nil {
			log.Fatal(err)
		}

		if err := client.ImportFrom(deviceid); err != nil {
			log.Fatal(err)
		}
	},
}
*/
