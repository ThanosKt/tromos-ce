/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package main // import "gitlab.com/tromos/tromos-ce/cmd/tromos-daemon"

import (
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

func newDaemonCommands() (*cobra.Command, *cobra.Command, error) {

	var tromosd *peer.Peer

	startCmd := &cobra.Command{
		Use:          "start  [OPTIONS]",
		Short:        "run the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		//DisableFlagsInUseLine: true,
		//Args:                  cli.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {

			peer, err := peer.New()
			if err != nil {
				return err
			}
			tromosd = peer

			if err := peer.RunWebservice(); err != nil {
				return err
			}

			log.User("Daemon succesfully terminated")
			return nil
		},
	}

	// Link the startCommand with the daemon cli
	RootCmd.AddCommand(startCmd)

	stopCmd := &cobra.Command{
		Use:          "stop  [OPTIONS]",
		Short:        "stop the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			if tromosd != nil {
				return tromosd.Shutdown()
			}
			return nil
		},
	}

	// Link the stopCommand with the daemon cli
	//RootCmd.AddCommand(stopCmd)

	return startCmd, stopCmd, nil
}
