/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package concurrency // import "gitlab.com/tromos/tromos-ce/pkg/concurrency"

import (
	"context"
	"math"
	"strconv"
	"sync"
	"sync/atomic"
)

// TimeoutCond is a sync.Cond  improve for support wait timeout.
type TimeoutCond struct {
	L          sync.Locker
	signal     chan int
	hasWaiters uint64
}

// NewTimeoutCond return a new TimeoutCond
func NewTimeoutCond(l sync.Locker) *TimeoutCond {
	cond := TimeoutCond{L: l, signal: make(chan int)}
	return &cond
}

func (cond *TimeoutCond) addWaiter() {
	v := atomic.AddUint64(&cond.hasWaiters, 1)
	if v == 0 {
		panic("too many waiters; max is " + strconv.FormatUint(math.MaxUint64, 10))
	}
}

func (cond *TimeoutCond) removeWaiter() {
	// Decrement. See notes here: https://godoc.org/sync/atomic#AddUint64
	v := atomic.AddUint64(&cond.hasWaiters, ^uint64(0))

	if v == math.MaxUint64 {
		panic("removeWaiter called more than once after addWaiter")
	}
}

// HasWaiters queries whether any goroutine are waiting on this condition
func (cond *TimeoutCond) HasWaiters() bool {
	return atomic.LoadUint64(&cond.hasWaiters) > 0
}

// Wait waits for a signal, or for the context do be done. Returns true if signaled.
func (cond *TimeoutCond) Wait(ctx context.Context) bool {
	cond.addWaiter()
	//copy signal in lock, avoid data race with Interrupt
	ch := cond.signal
	//wait should unlock mutex,  if not will cause deadlock
	cond.L.Unlock()
	defer cond.removeWaiter()
	defer cond.L.Lock()

	select {
	case _, ok := <-ch:
		return !ok
	case <-ctx.Done():
		return false
	}
}

// Signal wakes one goroutine waiting on c, if there is any.
func (cond *TimeoutCond) Signal() {
	select {
	case cond.signal <- 1:
	default:
	}
}

// Interrupt goroutine wait on this TimeoutCond
func (cond *TimeoutCond) Interrupt() {
	cond.L.Lock()
	defer cond.L.Unlock()
	close(cond.signal)
	cond.signal = make(chan int)
}
