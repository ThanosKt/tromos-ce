/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package concurrency // import "gitlab.com/tromos/tromos-ce/pkg/concurrency"

import "sync"

func NewAtomicSetter() *AtomicSetter {
	return &AtomicSetter{
		finalCh: make(chan struct{}),
	}
}

type AtomicSetter struct {
	locker  sync.Mutex
	final   bool
	finalCh chan struct{}
}

func (a *AtomicSetter) IsFinal() bool {
	a.locker.Lock()
	defer a.locker.Unlock()

	return a.isFinal()
}

func (a *AtomicSetter) isFinal() bool {
	select {
	case <-a.finalCh:
		return true
	default:
		return false
	}

}

func (a *AtomicSetter) Set(protected func()) {
	a.locker.Lock()
	defer a.locker.Unlock()

	if a.isFinal() {
		panic("Setting value on finalized context")
	}
	protected()
}

func (a *AtomicSetter) Finalize(protected func()) {

	a.locker.Lock()
	defer a.locker.Unlock()

	if a.final {
		panic("Value has been already finalized")
	}

	if protected != nil {
		protected()
	}

	a.final = true
	defer close(a.finalCh)
}

func (a *AtomicSetter) WaitFinal() {
	<-a.finalCh
}
