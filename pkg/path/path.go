/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package path // import "gitlab.com/tromos/tromos-ce/pkg/path"

import (
	//	"github.com/docker/docker/pkg/idtools"
	//"golang.org/x/sys/unix"
	"os"
	"path/filepath"
)

const FilePathSeparator = string(filepath.Separator)

// Handle some relative paths
func Normalize(path string) string {
	path = filepath.Clean(path)

	switch path {
	case ".":
		return FilePathSeparator
	case "..":
		return FilePathSeparator
	case "*":
		return ""
	default:
		return path
	}
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// CreateIfNotExists creates a file or a directory only if it does not already exist.
func CreateIfNotExists(path string, isDir bool) error {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			if isDir {
				return os.MkdirAll(path, 0755)
			}
			if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
				return err
			}
			f, err := os.OpenFile(path, os.O_CREATE, 0755)
			if err != nil {
				return err
			}
			f.Close()
		}
	}
	return nil
}

// Cleanup makes sure to provide a new directory free of stale data
func Cleanup(path string) error {
	/*
		if err := unix.Access(path, unix.W_OK); err != nil {
			return err
		}
	*/
	err := os.Remove(path)
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}
	return nil
}

/*
// PrepareDir prepares and returns the default directory to for temporary files.
// If it doesn't exist, it is created. If it exists, its content is removed.
func PrepareDir(rootDir string, subdir string) (string, error) {
	idMapping, err := idtools.NewIdentityMapping("tromos", "tromos")
	if err != nil {
		return "", err
	}

	rootIdentity := idMapping.RootPair()

	var tmpDir string
	tmpDir = filepath.Join(rootDir, subdir)
	newName := tmpDir + "-old"
	if err := os.Rename(tmpDir, newName); err == nil {
		go func() {
			if err := os.RemoveAll(newName); err != nil {
				panic(err)
			}
		}()
	} else if !os.IsNotExist(err) {
		if err := os.RemoveAll(tmpDir); err != nil {
			return "", err
		}
	}

	// We don't remove the content of tmpdir if it's not the default,
	// it may hold things that do not belong to us.
	return tmpDir, os.MkdirAll(tmpDir, 0700)
}
*/
