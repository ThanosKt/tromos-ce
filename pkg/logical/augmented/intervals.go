/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package augmented // import "gitlab.com/tromos/tromos-ce/pkg/logical/augmented"

import (
	"encoding/binary"
	"fmt"
	"github.com/workiva/go-datastructures/augmentedtree"
)

type dimension struct {
	low, high int64
}

type interval struct {
	dimensions []*dimension
	id         uint64
}

func (i *interval) CheckDimension(dimension uint64) {
	if dimension > uint64(len(i.dimensions)) {
		panic(fmt.Sprintf(`Dimension :%d out of range.`, dimension))
	}
}

func (i *interval) LowAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].low
}

func (i *interval) HighAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].high
}

/* Partial overlap */
func (i *interval) OverlapsAtDimension(iv augmentedtree.Interval, dimension uint64) bool {
	return i.HighAtDimension(dimension) >= iv.LowAtDimension(dimension) &&
		i.LowAtDimension(dimension) <= iv.HighAtDimension(dimension)
}

func (i *interval) ID() uint64 {
	return i.id
}

func (i *interval) Key() []byte {
	key := make([]byte, 8)
	binary.LittleEndian.PutUint64(key, i.id)
	return key
}

func constructSingleDimension(low, high int64, id uint64) *interval {
	return &interval{[]*dimension{{low: low, high: high}}, id}
}
