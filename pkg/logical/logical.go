/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package logical // import "gitlab.com/tromos/tromos-ce/pkg/logical"

import (
	"gitlab.com/tromos/tromos-ce/hub/logical"
)

type DefaultLogical struct {
	length uint64
}

func (df *DefaultLogical) Add(offset int64, size int64) uint64 {
	if uint64(size) > df.length {
		df.length = uint64(size)
	}
	return 0
}

func (df *DefaultLogical) Overlaps(offset int64, bytes int64) (size uint64, segments []logical.Segment) {

	return df.length, []logical.Segment{{To: int64(df.length)}}
}

func (df *DefaultLogical) Length() int64 {
	return int64(df.length)
}
