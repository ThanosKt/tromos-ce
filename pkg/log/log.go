/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package log // import "gitlab.com/tromos/tromos-ce/pkg/log"

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
)

var ShowAdmin bool = true
var ShowUser bool = true
var ShowTrace bool = false

var logger = logrus.New()

// Fields wraps logrus.Fields, which is a map[string]interface{}
type Fields logrus.Fields

func init() {
	//logger.Formatter = new(logrus.JSONFormatter)
	logger.Formatter = new(logrus.TextFormatter)
	logger.Level = logrus.DebugLevel
	logrus.SetOutput(os.Stdout)
}

func FileInfo(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		slash := strings.LastIndex(file, " tromos")
		if slash >= 0 {
			file = file[slash+1:]
		}
	}
	return fmt.Sprintf("%s:%d", file, line)
}

/* Issues related to the administrator. This may include
 * connection errors, bad lookups ... */
func Admin(args ...interface{}) {
	if ShowAdmin {
		entry := logger.WithFields(logrus.Fields{})
		entry.Data["file"] = FileInfo(2)
		entry.Error(args...)
	}
}

/* Issues related to the User. This may include
 * who started a transaction, when, how many data ... */
func User(args ...interface{}) {
	if ShowUser {
		entry := logger.WithFields(logrus.Fields{})
		entry.Data["file"] = FileInfo(2)
		entry.Info(args...)
	}
}

/* Whatever doesn't match the previous categories
 * goes here */
func Trace(args ...interface{}) {
	if ShowTrace {
		entry := logger.WithFields(logrus.Fields{})
		entry.Debug(args...)
	}
}
