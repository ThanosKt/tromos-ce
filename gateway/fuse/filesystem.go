/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
)

var storage *middleware.Client

type FilesystemConfig struct {
	Conn   *fuse.Conn
	Client *middleware.Client
}

func Mount(conf FilesystemConfig) error {

	storage = conf.Client

	if p := conf.Conn.Protocol(); !p.HasInvalidate() {
		panic("Kernel fuse support is too old to have invalidation")
	}

	srv := fs.New(conf.Conn, &fs.Config{
		/*
			Debug: func(msg interface{}) {
				log.Admin(msg)
			},
		*/
	})

	return srv.Serve(&Filesystem{
		RootNode: &Directory{
			name:     "root",
			children: make(map[string]interface{}),
		},
	})
}

type Filesystem struct {
	RootNode *Directory
}

// To be used by the main.go of the library
func (fs *Filesystem) Root() (fs.Node, error) {
	return fs.RootNode, nil
}
