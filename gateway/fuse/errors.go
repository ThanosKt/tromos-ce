/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

// Convert Tromos errors to Fuse errors
func MaskError(err error) error {
	switch err {
	case nil:
		return nil

	case coordinator.ErrBackend:
		return fuse.ENOSYS

	case coordinator.ErrNoImpl:
		return fuse.ENOSYS

	case coordinator.ErrKey:
		return fuse.EIO

	case coordinator.ErrNoExist:
		return fuse.ENOENT

	default:
		log.Admin("Uncaptured error:", err)
		return err
	}
}
