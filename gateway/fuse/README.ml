mode-string  | truncate? | create? | what's allowed
-------------+-----------+---------+----------------
     r       |    no     |  no     |  reading (only)
     w       |    yes    |  yes    |  writing (only)
     a       |    no     |  yes    |  writing (only), auto-appends
     r+      |    no     |  no     |  read and write
     w+      |    yes    |  yes    |  read and write
     a+      |    no     |  yes    |  read and (auto-appending) write
