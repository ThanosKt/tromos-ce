/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package random // import "gitlab.com/tromos/tromos-ce/hub/selector/random/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"math/rand"
)

func New(conf *viper.Viper) selector.Selector {
	return &Random{
		flat: []selector.SelectorProperties{},
	}
}

type Random struct {
	flat []selector.SelectorProperties
}

func (s *Random) Add(p selector.SelectorProperties) {
	s.flat = append(s.flat, p)
}

func (s *Random) Commit() {
}

func (s *Random) Walk(walkfn func(k []byte, v interface{}) bool) {

	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *Random) Partition(key string) (string, error) {
	panic("Not implemented")
}

func (s *Random) Select(exclude []string, _ ...selector.Capability) (string, error) {

	if len(s.flat) <= len(exclude) {
		return "", selector.ErrExhausted
	}

	// Continuously try until find one
	for {
		id := s.flat[rand.Int()%len(s.flat)].ID
		if !structures.StringInSlice(id, exclude) {
			return id, nil
		}
	}
}
