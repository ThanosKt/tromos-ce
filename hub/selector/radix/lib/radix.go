/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package radix // import "gitlab.com/tromos/tromos-ce/hub/selector/radix/lib"

import (
	"github.com/hashicorp/go-immutable-radix"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(conf *viper.Viper) selector.Selector {
	return &Radix{
		entries: iradix.New().Txn(),
	}
}

type Radix struct {
	entries *iradix.Txn
}

func (s *Radix) Deterministic() bool {
	return true
}

func (s *Radix) Add(p selector.SelectorProperties) {
	s.entries.Insert([]byte(p.ID), p)
}

func (s *Radix) Commit() {
	s.entries.Commit()
}

func (s *Radix) Walk(walkfn func(k []byte, v interface{}) bool) {
	s.entries.Root().Walk(walkfn)
}

func (s *Radix) Partition(key string) (string, error) {
	k, _, ok := s.entries.Root().LongestPrefix([]byte(key))
	if !ok {
		k, _, ok := s.entries.Root().Minimum()
		if !ok {
			return "", selector.ErrNoElement
		}
		return string(k), nil
	}
	return string(k), nil
}

func (s *Radix) Select(exclude []string, _ ...selector.Capability) (string, error) {
	panic("not implemented")
}
