/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package selector // import "gitlab.com/tromos/tromos-ce/hub/selector"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrSelector = errors.NewClass("Selector Error")
	ErrArg      = ErrSelector.NewClass("Argument error")
	ErrRuntime  = ErrSelector.NewClass("Runtime error")

	// Argument family errors
	ErrNoCriteria = ErrArg.New("No criteria have been defined")

	// Runtime family errors
	ErrExhausted = ErrRuntime.New("Elements in the pool have been exhausted")
	ErrNoElement = ErrRuntime.New("No matching element was found")
	ErrNoInit    = ErrRuntime.New("Selector has not been initialized")
)
