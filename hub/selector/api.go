/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package selector // import "gitlab.com/tromos/tromos-ce/hub/selector"

import (
	"github.com/spf13/viper"
)

type SelectorPlugin func(conf *viper.Viper) Selector

type SelectorProperties struct {
	ID           string
	Capabilities []Capability
	Peer         string
}

type Selector interface {
	// Add includes a new entity in the pool
	Add(p SelectorProperties)

	// Commit is used when all entities have been included. Select and
	// Walk can be used unly after a selector has been committed
	Commit()

	// Partition returns the authority responsible for the element
	Partition(key string) (string, error)

	// Select one of the available entities in the pool based on the
	// given constrains.
	Select(exclude []string, criteria ...Capability) (string, error)

	// Walk is used when iterating the tree. Returns if iterations should
	// be terminated
	Walk(func(k []byte, v interface{}) bool)
}
