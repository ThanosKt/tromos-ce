/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package logical // import "gitlab.com/tromos/tromos-ce/hub/logical"

type Segment struct {
	Index uint64
	From  int64
	To    int64
}

// Logical (files) does not contain data. It contains a description of records found in one or more physical files.
type Logical interface {
	// Add adds a new update record to the tree. Offset and size indicate the affected segment of the logical file
	Add(offset int64, size int64) uint64

	// Overlap returns the indices of deltas that overlap with the request
	// When request offset is not aligned with deltas, additional bytes
	// need to be transferred. We must calculate this overhead to the buffer
	Overlaps(from int64, to int64) (size uint64, segments []Segment)

	// Length returns the contiguous space (with holes) of records
	Length() int64
}
