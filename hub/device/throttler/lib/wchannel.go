/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package throttler // import "gitlab.com/tromos/tromos-ce/hub/device/throttler/lib"

import (
	"github.com/juju/ratelimit"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
)

// Allows only for one stream
type wchannel struct {
	throttle *Throttle
	bucket   *ratelimit.Bucket
	remote   device.WriteChannel
}

func (ch *wchannel) Close() error {
	return ch.remote.Close()
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, stream *device.Stream) error {
	bucket := ch.bucket
	if ch.throttle.regulate == Stream {
		bucket = ratelimit.NewBucketWithRate(ch.throttle.rate, ch.throttle.capacity)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, stream); err != nil {
		return err
	}

	go func() {
		if _, err := io.Copy(pw, ratelimit.Reader(src, bucket)); err != nil {
			panic(err)
		}
		if err := pw.Close(); err != nil {
			panic(err)
		}
	}()

	return nil
}
