/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package throttler // import "gitlab.com/tromos/tromos-ce/hub/device/throttler/lib"

import (
	"code.cloudfoundry.org/bytefmt"
	"github.com/juju/ratelimit"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

// Define where to apply regulation
type Regulate int

const (
	Global Regulate = iota
	Channel
	Stream
)

func New(conf *viper.Viper) device.Device {
	r, err := bytefmt.ToBytes(conf.GetString("rate"))
	if err != nil {
		panic(err)
	}
	rate := float64(r)

	c, err := bytefmt.ToBytes(conf.GetString("capacity"))
	if err != nil {
		panic(err)
	}
	capacity := int64(c)

	var bucket *ratelimit.Bucket
	var regulate Regulate
	switch conf.GetString("regulate") {
	case "global":
		regulate = Global
		bucket = ratelimit.NewBucketWithRate(rate, capacity)
	case "channel":
		regulate = Channel
		bucket = nil // Will be defined in NewChannel
	case "stream":
		regulate = Stream
		bucket = nil // Will be defined in NewTransfer
	default:
		panic("Unknown bucket regulation")
	}

	return &Throttle{
		bucket:   bucket,
		rate:     rate,
		capacity: capacity,
		regulate: regulate,
	}
}

type Throttle struct {
	device.Device
	bucket   *ratelimit.Bucket
	regulate Regulate
	rate     float64
	capacity int64
}

func (throttle *Throttle) String() string {
	return "throttle <- " + throttle.Device.String()
}

func (throttle *Throttle) Capabilities() []selector.Capability {
	return append(throttle.Device.Capabilities(), selector.Ratecontrol)
}

func (throttle *Throttle) Location() string {
	return throttle.Device.Location()
}

func (throttle *Throttle) SetBackend(backend device.Device) {
	if backend == nil {
		panic("NIL Device not allowed")
	}
	throttle.Device = backend
}

func (throttle *Throttle) NewWriteChannel(name string) (device.WriteChannel, error) {

	bucket := throttle.bucket
	if throttle.regulate == Channel {
		bucket = ratelimit.NewBucketWithRate(throttle.rate, throttle.capacity)
	}

	channel, err := throttle.Device.NewWriteChannel(name)
	if err != nil {
		return nil, err
	}
	return &wchannel{
		throttle: throttle,
		bucket:   bucket,
		remote:   channel,
	}, nil
}

func (throttle *Throttle) NewReadChannel(name string) (device.ReadChannel, error) {
	channel, err := throttle.Device.NewReadChannel(name)
	if err != nil {
		return nil, err
	}

	return &rchannel{
		ReadChannel: channel,
	}, nil
}

func (throttle *Throttle) Close() error {
	return throttle.Device.Close()
}
