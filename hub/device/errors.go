/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package device // import "gitlab.com/tromos/tromos-ce/hub/device"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrDevice  = errors.NewClass("Device Error")
	ErrArg     = ErrDevice.NewClass("Argument error")
	ErrRuntime = ErrDevice.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrDevice.New("Backend error")
	ErrNoImpl  = ErrDevice.New("Function not implemented")
	ErrProxy   = ErrDevice.New("Uncaptured PROXY ERROR")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrChannelClosed = ErrRuntime.New("Action on closed channel")
	ErrStream        = ErrRuntime.New("Stream transfer error")
)
