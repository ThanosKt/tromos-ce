/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package filesystem // import "gitlab.com/tromos/tromos-ce/hub/device/filesystem/lib"

import (
	"github.com/spf13/afero"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
)

type rchannel struct {
	afero.Fs
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	file, err := ch.Fs.Open(in.Item.ID)
	if err != nil {
		return err
	}
	/* -- Question: is it better to fetch the chunks into memory
	* or to directly access the medium ?
		if _, err := file.Seek(item.Offset, 0); err != nil {
			return err
		}

		if _, err := io.CopyN(dst, file, item.Size); err != nil {
			return err
		}
	*/
	go func() {
		defer dst.Close()
		defer file.Close()

		reader := io.NewSectionReader(file, int64(in.Item.Offset), int64(in.Item.Size))
		if _, err := io.Copy(dst, reader); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		if err := file.Close(); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
	}()
	return nil
}
