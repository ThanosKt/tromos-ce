/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package filesystem // import "gitlab.com/tromos/tromos-ce/hub/device/filesystem/lib"

import (
	"fmt"
	"github.com/spf13/afero"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type transfer struct {
	in  *device.Stream
	out afero.File
}

type wchannel struct {
	afero.Fs

	prefix string

	transferLocker sync.Mutex
	transfers      []*transfer

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	for _, transfer := range ch.transfers {
		<-transfer.in.Complete
	}
	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, in *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pid := len(ch.transfers)

	// Create a data holder
	filepath := fmt.Sprintf("%v.%v", ch.prefix, pid)
	file, err := ch.Fs.Create(filepath)
	if err != nil {
		return err
	}

	transfer := &transfer{
		in:  in,
		out: file,
	}
	ch.transfers = append(ch.transfers, transfer)

	// Perform the data transfer asynchronously
	go func() {
		wb, err := io.Copy(transfer.out, src)
		if err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
		if err := transfer.out.Sync(); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
		if err := transfer.out.Close(); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		transfer.in.Item.Size = uint64(wb)
		transfer.in.Item.ID = filepath

		close(transfer.in.Complete)
	}()
	return nil
}
