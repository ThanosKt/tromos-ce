/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package googledrive // import "gitlab.com/tromos/tromos-ce/hub/device/googledrive/lib"

import (
	"bytes"
	"fmt"
	"github.com/miolini/datacounter"
	"github.com/prasmussen/gdrive/drive"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"io/ioutil"
	"strings"
	"sync"
)

type wchannel struct {
	gd     *Googledrive
	prefix string

	transferLocker sync.Mutex
	transfers      []*device.Stream

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	for _, transfer := range ch.transfers {
		<-transfer.Complete
	}
	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, stream *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pid := len(ch.transfers)

	ch.transfers = append(ch.transfers, stream)

	go func() {
		counter := datacounter.NewReaderCounter(src)

		// STUPID $#$# DRIVER.
		// ID is parsed from the output
		// For the bytes we have an intermediate counter
		key := fmt.Sprintf("%v.%v", ch.prefix, pid)
		response := bytes.NewBufferString("")
		req := drive.UploadStreamArgs{
			Out:  response,
			In:   counter,
			Name: key,
			//Parents:     []string{args.ParentID},
			Mime:  "octet-stream",
			Share: false,
			//		ChunkSize: bufLen,
			Progress: ioutil.Discard,
		}
		if err := ch.gd.driver.UploadStream(req); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		lines := strings.Split(response.String(), "\n")
		id := strings.Split(lines[1], " ")[1]
		if len(id) == 0 {
			if err := src.CloseWithError(device.ErrStream); err != nil {
				panic(err)
			}
		}
		stream.Item.ID = id
		stream.Item.Size = uint64(counter.Count())

		close(stream.Complete)
	}()
	return nil
}
