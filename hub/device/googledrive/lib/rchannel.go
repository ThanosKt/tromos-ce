/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package googledrive // import "gitlab.com/tromos/tromos-ce/hub/device/googledrive/lib"

import (
	"bytes"
	"github.com/prasmussen/gdrive/drive"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"io/ioutil"
)

type rchannel struct {
	gd    *Googledrive
	chunk *bytes.Buffer
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	if ch.chunk == nil {
		ch.chunk = bytes.NewBuffer(nil)

		// Retrieve the whole chunk (potentially blob)
		req := drive.DownloadArgs{
			Progress: ioutil.Discard,
			Out:      ch.chunk,
			Id:       in.Item.ID,
			Stdout:   true, // Write data to out (instead of file)
		}
		if err := ch.gd.driver.Download(req); err != nil {
			return err
		}
	}

	go func() {
		defer dst.Close()
		_, err := io.Copy(dst, io.NewSectionReader(bytes.NewReader(ch.chunk.Bytes()), int64(in.Item.Offset), int64(in.Item.Size)))
		if err != nil {
			panic(err)
		}

	}()
	return nil
}
