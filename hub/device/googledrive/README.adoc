= Google Plugin

== Description

Gives access to the Googledrive platform. To get your credentials and allow Tromos to access your account just follow link:https://www.iperiusbackup.net/en/how-to-enable-google-drive-api-and-get-client-credentials/[this excellent guide]



== Arguments

[cols="2,2,5a", options="header"]
|===
|Field
|Type
|Description


|credentials
|String
|Path to the json file that hold the bucket credentials 

|===

==== Example

[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: "googledrive"
            credentials: "/home/myuser/mycredentials.json"
        ..... Translators and Proxies ...
----

