/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package blob // import "gitlab.com/tromos/tromos-ce/hub/device/blob/lib"

import (
	"bytes"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel
}

func (ch *rchannel) Close() error {
	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	// Flatten the logical item into several physical items
	var chain []*device.Stream
	chain = append(chain, &device.Stream{Item: in.Item})
	chain[0].Item.Chain = nil

	for _, item := range in.Item.Chain {
		chain = append(chain, &device.Stream{Item: item})
	}

	var wg sync.WaitGroup
	bufs := make([]*bytes.Buffer, len(chain))
	for i, stream := range chain {
		pr, pw := io.Pipe()

		if err := ch.remote.NewTransfer(pw, stream); err != nil {
			return err
		}

		wg.Add(1)
		go func(i int, stream *device.Stream) {
			defer wg.Done()
			bufs[i] = bytes.NewBuffer(make([]byte, 0, stream.Item.Size))
			if _, err := io.CopyN(bufs[i], pr, int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}(i, stream)

	}

	go func() {
		wg.Wait()
		defer dst.Close()

		for i, stream := range chain {
			// Keep it like that. Otherwise EOF never comes and garbages return
			if _, err := io.CopyN(dst, bufs[i], int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}
	}()

	return nil
}
