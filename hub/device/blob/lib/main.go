/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package blob // import "gitlab.com/tromos/tromos-ce/hub/device/blob/lib"

import (
	"code.cloudfoundry.org/bytefmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(conf *viper.Viper) device.Device {

	bs, err := bytefmt.ToBytes(conf.GetString("blocksize"))
	if err != nil {
		panic(err)
	}

	return &Blob{blocksize: bs}
}

type Blob struct {
	device.Device
	blocksize uint64
}

func (blob *Blob) String() string {
	return "blob <- " + blob.Device.String()
}

func (blob *Blob) Capabilities() []selector.Capability {
	return append(blob.Device.Capabilities(), selector.BurstBuffer)
}

func (blob *Blob) SetBackend(backend device.Device) {
	if backend == nil {
		panic("NIL Device not allowed")
	}
	blob.Device = backend
}

func (blob *Blob) NewWriteChannel(name string) (device.WriteChannel, error) {
	channel, err := blob.Device.NewWriteChannel(name)
	if err != nil {
		return nil, err
	}
	return &wchannel{
		remote: channel,
		buffer: make([]byte, blob.blocksize),
	}, nil
}

func (blob *Blob) NewReadChannel(name string) (device.ReadChannel, error) {
	channel, err := blob.Device.NewReadChannel(name)
	if err != nil {
		return nil, err
	}
	return &rchannel{remote: channel}, nil
}

func (blob *Blob) Close() error {
	return blob.Device.Close()
}
