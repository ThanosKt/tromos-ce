/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package proxy // import "gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/client/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/protocol"
	"io"
)

type rchannel struct {
	remote *WebServiceOperations
	chanID string

	session *yamux.Session
}

func (ch *rchannel) Close() (err error) {
	errcode := ch.remote.RCH_Close(ch.chanID)
	return protocol.UnmaskError(errcode)
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, stream *device.Stream) error {
	errcode := ch.remote.RCH_WriteTo(ch.chanID, stream.Item)
	if err := protocol.UnmaskError(errcode); err != nil {
		return err
	}

	// Open a new stream
	go func() {
		defer dst.Close()
		conn, err := ch.session.Open()
		if err != nil {
			panic(err)
		}
		defer conn.Close()

		if _, err = io.Copy(dst, conn); err != nil {
			panic(err)
		}
	}()
	return nil
}
