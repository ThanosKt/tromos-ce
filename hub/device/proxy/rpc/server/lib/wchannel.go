/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package proxy // import "gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/server/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type transfer struct {
	in       *yamux.Stream
	out      *device.Stream
	complete chan error
}

type wchannel struct {
	remote device.WriteChannel

	transferLocker sync.Mutex
	transfers      []*transfer

	session *yamux.Session

	closed bool
}

func (ch *wchannel) Close() ([]device.Item, error) {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	// Drain the flying streams
	for _, transfer := range ch.transfers {
		if err := <-transfer.complete; err != nil {
			return nil, device.ErrStream
		}
	}

	// Prohibite future streams
	if err := ch.session.Close(); err != nil {
		return nil, err
	}

	// Get the backend metadata
	if err := ch.remote.Close(); err != nil {
		return nil, err
	}

	var metadata []device.Item
	for _, transfer := range ch.transfers {
		metadata = append(metadata, transfer.out.Item)
	}
	return metadata, nil
}

func (ch *wchannel) NewStream() error {

	ch.transferLocker.Lock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	// No error checking is needed. This is a client's task
	out := &device.Stream{Complete: make(chan struct{})}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, out); err != nil {
		return err
	}

	transfer := &transfer{
		out:      out,
		complete: make(chan error),
	}

	go func() {
		in, err := ch.session.AcceptStream()
		if err != nil {
			transfer.complete <- err
			return
		}
		transfer.in = in

		ch.transfers = append(ch.transfers, transfer)
		ch.transferLocker.Unlock()

		if _, err := io.Copy(pw, in); err != nil {
			transfer.complete <- err
			return
		}
		if err := pw.Close(); err != nil {
			transfer.complete <- err
			return
		}
		close(transfer.complete)
	}()
	return nil
}
