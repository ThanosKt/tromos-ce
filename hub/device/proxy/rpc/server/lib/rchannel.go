/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package proxy // import "gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/server/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel

	transferLocker sync.Mutex
	session        *yamux.Session

	closed bool
}

func (ch *rchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(stream *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pw, stream); err != nil {
		return err
	}

	go func() {
		in, err := ch.session.AcceptStream()
		if err != nil {
			panic(err)
		}

		if _, err := io.Copy(in, pr); err != nil {
			panic(err)
		}

		if err := in.Close(); err != nil {
			panic(err)
		}

	}()

	return nil
}
