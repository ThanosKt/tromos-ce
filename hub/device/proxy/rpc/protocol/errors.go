/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package protocol // import "gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/protocol"

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"log"
)

// errors is a interface, thus its scope is only local.
// to be able to propagate error we use masked error codes instead
type ErrCode int

const (
	CodeOK      ErrCode = 0
	CodeProxy           = 1
	CodeBackend         = 2
	CodeNoImpl          = 3
)

func MaskError(err error) ErrCode {
	switch err {
	case nil:
		return CodeOK

	case device.ErrBackend:
		return CodeBackend

	case device.ErrNoImpl:
		return CodeNoImpl

	default:
		log.Printf("Uncaptured error %v", err)
		return CodeProxy
	}
}

func UnmaskError(code ErrCode) error {
	switch code {
	case CodeOK:
		return nil

	case CodeBackend:
		return device.ErrBackend

	case CodeNoImpl:
		return device.ErrNoImpl

	default:
		return device.ErrProxy
	}
}
