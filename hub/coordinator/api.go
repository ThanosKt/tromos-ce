/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package coordinator // import "gitlab.com/tromos/tromos-ce/hub/coordinator"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

type Delta struct {
	// changes on the logical file (segment)
	Offset int `json:"Offset"`
	Size   int `json:"Size"`
	// changes on the processsing plane
	State map[string]interface{} `json:"State,omitempty"`
	// changes on the storage plane
	Items map[string]device.Item `json:"Items"`
}

type UpdateRecord struct {
	// List of Affected Devices
	Sinks map[string]string `json:"Sinks,omitempty"`
	// list of changes occurred within the transaction
	Deltas []Delta `json:"Deltas,omitempty"`
}

type CoordinatorPlugin func(conf *viper.Viper) (Coordinator, error)

type IntentionRecord []byte

// Coordinator represents a K/V store for storring logical files.
// It consists out of several layers with common API.
// Every logical file is a tuple of a transaction log and an update log
//
// In the current API it does not attempt to guarantee durability in the sense that once the
// log write completes the data is guaranteed to be on stable storage
type Coordinator interface {
	// SetBackend defines the backend for the current device layer
	SetBackend(Coordinator)

	// String returns a descriptor for the current device layer
	String() string

	// Capabilities returns the capabilities of the Coordinator
	Capabilities() []selector.Capability

	// Location returns the node where the Coordinator is running
	Location() string

	// Close terminates the coordinator
	Close() error

	Info(key string) ([][]byte, Info, error)

	// CreateOrReset creates a new logical file if it does not exist.
	// If it exists, it resets all of the contents
	CreateOrReset(key string) error

	// CreateIfNotExist creates a new logical file if it does not exists.
	// If it exists, it returns an error
	CreateIfNotExist(key string) error

	// SetLandmark appends a landmark to the update log.
	SetLandmark(key string, mark Landmark) error

	// UpdateStart initiates a new transaction for writing to the logical file.
	// It appends the intentions to the transaction log of the logical file
	// identified by key
	UpdateStart(key string, tid string, intentions IntentionRecord) error

	// UpdateEnd appends a new entry on the update log to store
	// metadata of a client transaction (tid). Once the write is complete, it atomically
	// removes the respective entry from the transaction log
	UpdateEnd(key string, tid string, record []byte) error

	// ViewStart returns all the records (see landmark) for a logical file
	// and keeps a lease to protect them from being garbage collected
	ViewStart(key string, filter []string) (records [][]byte, tids []string, err error)

	// ViewEnd removes the leases allocated during ViewStart
	ViewEnd(tids []string) error
}

// Landmark is an instruction to the Coordinator as to what records to
// return when getting the information for a logical file
type Landmark struct {
	// IgnorePrevious makes the Coordinator to stop iterating records of the
	// logical file and return immediately. Used to imitate truncate operation
	IgnorePrevious bool
	// Disappear makes the Coordinator to reply as if the logical file does not exists
	// Used to imitate the behavior of a remove operation
	Disappear bool
}

type Info map[string]string
