/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package proxy // import "gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/client/lib"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/protocol"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"sync"
	"time"
)

type WebServiceOperations struct {
	ProxyString           func() string
	ProxyCapabilities     func() []selector.Capability
	ProxyInfo             func(key string) ([][]byte, coordinator.Info, protocol.ErrCode)
	ProxyCreateOrReset    func(key string) protocol.ErrCode
	ProxyCreateIfNotExist func(key string) protocol.ErrCode
	ProxySetLandmark      func(key string, mark coordinator.Landmark) protocol.ErrCode
	ProxyUpdateStart      func(key string, tid string, ir coordinator.IntentionRecord) protocol.ErrCode
	ProxyUpdateEnd        func(key string, tid string, ur []byte) protocol.ErrCode
	ProxyViewStart        func(key string, filter []string) ([][]byte, []string, protocol.ErrCode)
	ProxyViewEnd          func(history []string) protocol.ErrCode
	ProxyClose            func() error
}

type proxyclient struct {
	*WebServiceOperations
	conn *rpc.WebSocketClient
}

var once sync.Once
var proxyClientSingleton *proxyclient

func New(conf *viper.Viper) (coordinator.Coordinator, error) {
	logger := logrus.WithField("module", "proxy/rpc/client")

	host := conf.GetString("host")
	if host == "" {
		logrus.Errorf("Host field is empty")
		return nil, coordinator.ErrInvalid
	}

	port := conf.GetString("port")
	if port == "" {
		logrus.Errorf("Port field is empty")
		return nil, coordinator.ErrInvalid
	}

	timeout, err := time.ParseDuration(conf.GetString("timeout"))
	if err != nil {
		return nil, err
	}

	return &Client{
		logger:  logger,
		host:    host,
		port:    port,
		timeout: timeout,
	}, nil
}

type Client struct {
	logger  *logrus.Entry
	host    string
	port    string
	timeout time.Duration
}

func (cli *Client) remote() *proxyclient {
	once.Do(func() {
		// otherwise start a new instance
		call := &WebServiceOperations{}
		conn := rpc.NewWebSocketClient("ws://" + cli.host + ":" + cli.port + "/")
		conn.SetTimeout(cli.timeout)
		conn.UseService(call)

		proxyClientSingleton = &proxyclient{WebServiceOperations: call, conn: conn}
	})
	return proxyClientSingleton
}

func (cli *Client) SetBackend(_ coordinator.Coordinator) {
}

func (cli *Client) Close() error {
	return cli.remote().ProxyClose()
}

func (cli *Client) String() string {
	return cli.remote().ProxyString()
}

func (cli *Client) Location() string {
	return cli.host
}

func (cli *Client) Capabilities() []selector.Capability {
	return cli.remote().ProxyCapabilities()
}

func (cli *Client) Info(key string) ([][]byte, coordinator.Info, error) {
	history, info, err := cli.remote().ProxyInfo(key)
	return history, info, protocol.UnmaskError(err)
}

func (cli *Client) CreateOrReset(key string) error {
	err := cli.remote().ProxyCreateOrReset(key)
	return protocol.UnmaskError(err)
}

func (cli *Client) CreateIfNotExist(key string) error {
	err := cli.remote().ProxyCreateIfNotExist(key)
	return protocol.UnmaskError(err)
}

func (cli *Client) SetLandmark(key string, mark coordinator.Landmark) error {
	err := cli.remote().ProxySetLandmark(key, mark)
	return protocol.UnmaskError(err)
}

func (cli *Client) UpdateStart(key string, tid string, ir coordinator.IntentionRecord) error {
	err := cli.remote().ProxyUpdateStart(key, tid, ir)
	return protocol.UnmaskError(err)
}

func (cli *Client) UpdateEnd(key string, tid string, ur []byte) error {
	err := cli.remote().ProxyUpdateEnd(key, tid, ur)
	return protocol.UnmaskError(err)
}

func (cli *Client) ViewStart(key string, filter []string) ([][]byte, []string, error) {
	records, tids, err := cli.remote().ProxyViewStart(key, filter)
	return records, tids, protocol.UnmaskError(err)
}

func (cli *Client) ViewEnd(history []string) error {
	err := cli.remote().ProxyViewEnd(history)
	return protocol.UnmaskError(err)
}
