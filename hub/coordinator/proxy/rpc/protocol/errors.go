/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package protocol // import "gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/protocol"

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"log"
)

// errors is a interface, thus its scope is only local.
// to be able to propagate error we use masked error codes instead
type ErrCode int

const (
	CodeOK         ErrCode = 0
	CodeProxy              = 1
	CodeErrBackend         = 2
	CodeErrNoImpl          = 3
	CodeErrKey             = 4
	CodeErrNoExist         = 5
)

func MaskError(err error) ErrCode {
	switch err {
	case nil:
		return CodeOK

	case coordinator.ErrBackend:
		return CodeErrBackend

	case coordinator.ErrNoImpl:
		return CodeErrNoImpl

	case coordinator.ErrKey:
		return CodeErrKey

	case coordinator.ErrNoExist:
		return CodeErrNoExist

	default:
		log.Printf("Uncaptured error %v", err)
		return CodeProxy
	}
}

func UnmaskError(code ErrCode) error {
	switch code {
	case CodeOK:
		return nil

	case CodeErrBackend:
		return coordinator.ErrBackend

	case CodeErrNoImpl:
		return coordinator.ErrNoImpl

	case CodeErrKey:
		return coordinator.ErrKey

	case CodeErrNoExist:
		return coordinator.ErrNoExist

	default:
		return coordinator.ErrProxy
	}
}
