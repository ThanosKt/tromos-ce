/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package proxy // import "gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/server/lib"

import (
	"context"
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/protocol"
	"net/http"
)

func New(conf *viper.Viper) (coordinator.Coordinator, error) {
	logger := logrus.WithField("module", "proxy/rpc/server")

	host := conf.GetString("host")
	if host == "" {
		logrus.Errorf("Host field is empty")
		return nil, coordinator.ErrInvalid
	}

	port := conf.GetString("port")
	if port == "" {
		logrus.Errorf("Port field is empty")
		return nil, coordinator.ErrInvalid
	}

	return &Server{
		logger: logger,
		host:   host,
		port:   port,
	}, nil
}

type Server struct {
	logger *logrus.Entry
	coordinator.Coordinator
	host       string
	port       string
	webservice *http.Server
}

func (srv *Server) SetBackend(backend coordinator.Coordinator) {
	srv.Coordinator = backend

	// Lazy initialization - if there is no backend, there is no point in
	// running a server
	ops := rpc.NewWebSocketService()
	ops.AddFunction("ProxyString", backend.String)
	ops.AddFunction("ProxyCapabilities", backend.Capabilities)
	ops.AddFunction("ProxyCreateOrReset", srv.ProxyCreateOrReset)
	ops.AddFunction("ProxyInfo", srv.ProxyInfo)
	ops.AddFunction("ProxyCreateIfNotExist", srv.ProxyCreateIfNotExist)
	ops.AddFunction("ProxySetLandmark", srv.ProxySetLandmark)
	ops.AddFunction("ProxyUpdateStart", srv.ProxyUpdateStart)
	ops.AddFunction("ProxyUpdateEnd", srv.ProxyUpdateEnd)
	ops.AddFunction("ProxyViewStart", srv.ProxyViewStart)
	ops.AddFunction("ProxyViewEnd", srv.ProxyViewEnd)
	ops.AddFunction("ProxyClose", srv.Close)

	webservice := &http.Server{
		Addr:    srv.host + ":" + srv.port,
		Handler: ops,
	}
	srv.webservice = webservice

	go func() {
		if err := webservice.ListenAndServe(); err != nil {
			panic(err)
		}
	}()
}

func (srv *Server) Close() error {
	if err := srv.webservice.Shutdown(context.TODO()); err != nil {
		return err
	}
	return srv.Coordinator.Close()
}

func (srv *Server) ProxyInfo(key string) ([][]byte, map[string]string, protocol.ErrCode) {
	history, info, err := srv.Coordinator.Info(key)
	return history, info, protocol.MaskError(err)
}

func (srv *Server) ProxyCreateOrReset(key string) protocol.ErrCode {
	err := srv.Coordinator.CreateOrReset(key)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyCreateIfNotExist(key string) protocol.ErrCode {
	err := srv.Coordinator.CreateIfNotExist(key)
	return protocol.MaskError(err)
}

func (srv *Server) ProxySetLandmark(key string, mark coordinator.Landmark) protocol.ErrCode {
	err := srv.Coordinator.SetLandmark(key, mark)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyUpdateStart(key string, tid string, ir coordinator.IntentionRecord) protocol.ErrCode {
	err := srv.Coordinator.UpdateStart(key, tid, ir)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyUpdateEnd(key string, tid string, ur []byte) protocol.ErrCode {
	err := srv.Coordinator.UpdateEnd(key, tid, ur)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyViewStart(key string, filter []string) ([][]byte, []string, protocol.ErrCode) {
	records, tids, err := srv.Coordinator.ViewStart(key, filter)
	return records, tids, protocol.MaskError(err)
}

func (srv *Server) ProxyViewEnd(history []string) protocol.ErrCode {
	err := srv.Coordinator.ViewEnd(history)
	return protocol.MaskError(err)
}
