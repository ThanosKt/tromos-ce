/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package coordinator // import "gitlab.com/tromos/tromos-ce/hub/coordinator"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrCoordinator = errors.NewClass("Coordinator Error")
	ErrArg         = ErrCoordinator.NewClass("Argument error")
	ErrRuntime     = ErrCoordinator.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrCoordinator.New("Backend error")
	ErrNoImpl  = ErrCoordinator.New("Function not implemented")
	ErrProxy   = ErrCoordinator.New("Uncaptured PROXY ERROR")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrKey        = ErrArg.New("Key error")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrNoExist = ErrRuntime.New("Key does not exist")
)
