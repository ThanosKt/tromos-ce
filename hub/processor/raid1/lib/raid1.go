/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package raid1 // import "gitlab.com/tromos/tromos-ce/hub/processor/raid1/lib"

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/echo"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/mirror"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
)

// Options
// Replicas: Number of replicas
// Encrypt: whether to encrypt the replicas
// IntegrityCheck: whether to implement integrirty checking
func New(config *viper.Viper) processor.ProcessGraph {

	g := &graph{}

	// Validate Integritycheck
	g.integritycheck = config.GetBool("integritycheck")

	// Validate Encryption
	g.encrypt = config.GetBool("encrypt")

	// Validate Passphrase
	g.passphrase = config.GetString("passphrase")

	// Validate replicas
	if config.GetInt("replicas") == 0 {
		panic("0 replicas are not allowed")
	} else {
		g.replicas = config.GetInt("replicas")
	}

	return g
}

type graph struct {
	replicas       int
	encrypt        bool
	integritycheck bool
	passphrase     string
}

func (*graph) Reusable() bool {
	return true
}

// Bottom-up description, with intermediate placeholders
func (graph *graph) Upstream(b processor.Builder) {

	b.Add("fw0", &echo.Uplink{})
	if graph.integritycheck {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("mirror", mirror.NewUplink())
	if graph.encrypt {
		b.Add("encrypter", aes.NewUplink(0, graph.passphrase))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")
	} else {
		b.Connect("fw0", "Out", "mirror", "In")
	}

	for i := 0; i < graph.replicas; i++ {
		echoID := fmt.Sprintf("echo%v", i)
		b.Add(echoID, &echo.Uplink{})
		b.Connect("mirror", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out")
	}
}

// Top-down description, without intermediate placeholders. Starting from the most
// specific to the more generic
func (graph *graph) Downstream(b processor.Builder) {

	b.Add("mirror", mirror.NewDownlink())
	switch {
	case graph.integritycheck && graph.encrypt:
		b.Add("checksum", sha256.NewDownlink())
		b.Add("encrypter", aes.NewDownlink(0, graph.passphrase))

		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")

	case graph.integritycheck:
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "mirror", "In")

	case graph.encrypt:
		b.Add("encrypter", aes.NewDownlink(0, graph.passphrase))
		b.MapInPort("In", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")
	}

	for i := 0; i < graph.replicas; i++ {
		echoID := fmt.Sprintf("echo%v", i)
		b.Add(echoID, &echo.Uplink{})
		b.Connect("mirror", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out")
	}
}
