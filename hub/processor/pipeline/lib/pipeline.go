/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package pipeline // import "gitlab.com/tromos/tromos-ce/hub/processor/pipeline/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/echo"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/mirror"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Pipeline{}
}

type Pipeline struct{}

func (d *Pipeline) Reusable() bool {
	return false
}

func (d *Pipeline) Upstream(b processor.Builder) {
	b.Add("feed", &echo.Uplink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewUplink())
	b.Connect("feed", "Out", "checksum", "In")
	b.Add("mirror", mirror.NewUplink())
	b.Connect("checksum", "out", "mirror", "in")

	b.Add("encrypt", aes.NewUplink(0, "lolabunnysixteen"))
	b.Connect("mirror", "Out", "encrypt", "In")

	b.Add("echo", &echo.Uplink{})
	b.Connect("encrypt", "Out", "echo", "In")

	b.MapOutPort("s0", "echo", "Out")

	b.Add("nilbranch", &echo.Uplink{})
	b.Connect("mirror", "Out", "nilbranch", "In")
	b.MapOutPort("s1", "nilbranch", "Out")
}

func (d *Pipeline) Downstream(b processor.Builder) {
	b.Add("feed", &echo.Downlink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewDownlink())
	b.Connect("feed", "Out", "checksum", "In")
	b.MapOutPort("s0", "checksum", "Out")
	b.Add("mirror", mirror.NewDownlink())
	b.Connect("checksum", "Out", "mirror", "In")

	b.Add("encrypt", aes.NewDownlink(0, "lolabunnysixteen"))
	b.Connect("mirror", "Out", "encrypt", "In")

	b.Add("echo", &echo.Downlink{})
	b.Connect("encrypt", "Out", "echo", "In")

	b.MapOutPort("s0", "echo", "Out")

	b.Add("nilbranch", &echo.Downlink{})
	b.Connect("mirror", "Out", "nilbranch", "In")
	b.MapOutPort("s1", "nilbranch", "Out")
}
