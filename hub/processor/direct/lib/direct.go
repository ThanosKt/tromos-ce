/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package direct // import "gitlab.com/tromos/tromos-ce/hub/processor/direct/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/echo"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Direct{}
}

type Direct struct{}

func (d *Direct) Reusable() bool {
	return true
}

func (d *Direct) Upstream(b processor.Builder) {
	b.Add("echo", &echo.Uplink{})

	b.MapInPort("In", "echo", "In")
	b.MapOutPort("s0", "echo", "Out")
}

func (d *Direct) Downstream(b processor.Builder) {
	b.Add("echo", &echo.Downlink{})

	b.MapInPort("In", "echo", "In")
	b.MapOutPort("s0", "echo", "Out")
}
