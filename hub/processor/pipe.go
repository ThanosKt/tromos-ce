/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package processor // import "gitlab.com/tromos/tromos-ce/hub/processor"

import (
	"io"
)

type ReadWriteCloser interface {
	io.ReadWriteCloser
	CloseWithError(err error) error
}

type Receiver struct {
	*io.PipeReader
}

func (r Receiver) Write(data []byte) (n int, err error) {
	panic("Write on receiver is not allowed")
}

type Sender struct {
	*io.PipeWriter
}

func (s Sender) Read(p []byte) (n int, err error) {
	panic("Read on sender is not allowed")
}

func Pipe() (ReadWriteCloser, ReadWriteCloser) {
	pr, pw := io.Pipe()
	return Receiver{pr}, Sender{pw}
}
