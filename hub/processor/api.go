/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package processor // import "gitlab.com/tromos/tromos-ce/hub/processor"

import (
	"context"
	"github.com/spf13/viper"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

// Module is a struct that must be embeded to all the components that want to
// be integerated into Processor. The Processor expects the module to triggger
// signals for all its connected ports. For modules like mirror whether some
// of the ports are not really used, the module must send to its outputs a
// closed pipe reader so for the Processor to gracefully terminate the line
type Module struct {
	flow.Component
	ID string
}

// Builder is used to compile a Meta-graph into a DAG
// A Meta-graph is a graph that defines all the possible datapaths that will be used
type Builder interface {
	// Add adds a new process with a given name to the DAG
	Add(name string, module interface{}) bool

	// MapInPort adds an inport to the DAG and maps it to a contained module's port.
	MapInPort(port, app, appPort string)

	// Connect connects a sender to a receiver module
	Connect(senderName, senderPort, receiverName, receiverPort string) bool

	// MapOutPort adds an outport to the DAG  and maps it to a contained module's port.
	// Capability contain linking information with the next element (Processor or Device)
	MapOutPort(port, app, appPort string, capabilities ...selector.Capability)
}

type ProcessorPlugin func(conf *viper.Viper) ProcessGraph

// ProcessGraph are the drivers of the Processors.
// Should be used only by the Graph plugin Developers
type ProcessGraph interface {
	// Reusable returns whether a datapath can be used from a pool of precompile instances
	// or it has to be compiled at runtime
	Reusable() bool
	Upstream(b Builder)
	Downstream(b Builder)
}

type ChannelConfig struct {
	Context  context.Context
	Writable bool
	Name     string
	Sinks    map[string]string
}

type ChannelOption func(*ChannelConfig)

// Writable sets a channel if is writtable. If left ommitted or be false, the created
// channel is view only
func Writable(upstream bool) ChannelOption {
	return func(c *ChannelConfig) {
		c.Writable = upstream
	}
}

// Name set the name of the collection that will be created on the device. If not used, a
// random name is picked
func Name(name string) ChannelOption {
	return func(c *ChannelConfig) {
		c.Name = name
	}
}

// Sinks assigns the echos to the channel. On downstrea, the caller must set the echos for
// the processor to read data from. On upstream, the processor will add the selected echos.
// If nil, it allocates a new map. If not nil, echos are appended to the existing map
func Sinks(echos map[string]string) ChannelOption {
	return func(c *ChannelConfig) {
		if echos == nil {
			c.Sinks = make(map[string]string)
		} else {
			c.Sinks = echos
		}
	}
}

// Processor is a continuously running instnace that spawn channels. It is the Device
// counterpart for the compute plane
type Processor interface {
	// String returns a descriptive name for the Processor
	String() string
	// Capabilities returns the capabilities of the Processor
	Capabilities() []selector.Capability
	// Location returns the node where the Processor is running
	Location() string
	// NewChannel initiates a new channel with the given option
	NewChannel(opts ...ChannelOption) (Channel, error)
	// NewChannelFromConfig initiates a new channel as specific in the configuration
	// (Needed to consistency forward configurations among processors)
	NewChannelFromConfig(config ChannelConfig) (Channel, error)
	// Close gracefully shutdowns the processor
	Close() error
}

type Channel interface {
	// Capabilities return tags of the output ports. Tags can be used to resolve compabibility
	// with the components to be linked (either Processors or Devices). Similarly, the can be
	// used as arguments in the device selection process. They are useful only for the upstream
	Capabilities(port string) []selector.Capability

	// ReadyPort returns a channel for listenting asynchronous events that occur on the ports
	// The Port of event corresponds to the index as returned of Outports()
	Readyports() chan *Stream

	// NewStream initiates an asynchronous transfer to the processor
	// The error indicates a mishappen during preparation phase.
	// Completion and errors are signalled back through pipe (see PipeError)
	NewTransfer(stream *Stream) error
	// Close blocks until all the pending streams are flushed and terminates
	// the channel. Second call to close should cause a panic. If there are any
	// errors occurred in the streams, Close() returns ErrStream. For the exact
	// error the caller must iterate the streams and use the Complete field to
	// extract the error code
	Close() error

	// Sinks returns all the devices affected by the datapath
	Sinks() map[string]string
}

// Stream is an end-to-end asynchronous transfer descriptor
type Stream struct {
	Data ReadWriteCloser `json:"-"` // Convey data
	// The port of the final DAG from which the stream reaches the Device
	Port string `json:"Port" validate:"nonzero,regexp=^[a-zA-Z0-9]*$"`

	Meta StreamMetadata `validate:"require"` // Convey metadata
}

// TransferState is an information packet for maintaining the state of the stream
type StreamMetadata struct {
	// State store changes on the processsing plane
	State map[string]string `json:"State,omitempty"`
	// Items store changes on the storage plane
	Items map[string]device.Item `json:"Items" validate:"nonzero"`
}
