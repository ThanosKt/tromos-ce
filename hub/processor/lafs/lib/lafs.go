/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package lafs // import "gitlab.com/tromos/tromos-ce/hub/processor/lafs/lib"

import (
	"code.cloudfoundry.org/bytefmt"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/reedsolomon"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/echo"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(config *viper.Viper) processor.ProcessGraph {

	lafs := &LAFS{}

	// Validate Integritycheck
	lafs.integritycheck = config.GetBool("integritycheck")

	// Validate Encryption
	lafs.encrypt = config.GetBool("encrypt")

	// Validate Passphrase
	lafs.passphrase = config.GetString("passphrase")

	// Validate datablocks
	if config.GetInt("datablocks") == 0 {
		panic("0 datablocks are not allowed")
	} else {
		lafs.datablocks = config.GetInt("datablocks")
	}

	// Validate parity blocks
	if config.GetInt("parityblocks") == 0 {
		panic("0 parityblocks are not allowed")
	} else {
		lafs.parityblocks = config.GetInt("parityblocks")
	}

	// Validate bloskzei
	bs, err := bytefmt.ToBytes(config.GetString("blocksize"))
	if err != nil {
		panic(err)
	}
	lafs.blocksize = int(bs)

	return lafs
}

type LAFS struct {
	integritycheck bool
	encrypt        bool
	passphrase     string
	datablocks     int
	parityblocks   int
	blocksize      int
}

func (*LAFS) Reusable() bool {
	return true
}

func (config *LAFS) Upstream(b processor.Builder) {
	b.Add("fw0", &echo.Uplink{})
	if config.integritycheck {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("erasure", reedsolomon.NewUplink(
		config.datablocks,
		config.parityblocks,
		config.blocksize,
	))
	if config.encrypt {
		b.Add("encrypter", aes.NewUplink(0, config.passphrase))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "erasure", "In")
	} else {
		b.Connect("fw0", "Out", "erasure", "In")
	}

	var dataechos int
	for i := 0; i < config.datablocks; i++ {
		echoID := fmt.Sprintf("echo%v", i)
		b.Add(echoID, &echo.Uplink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.DataStream)
		dataechos = i
	}

	for i := 0; i < config.parityblocks; i++ {
		echoID := fmt.Sprintf("echo%v", i+dataechos+1)
		b.Add(echoID, &echo.Uplink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.ParityStream)
	}
}

func (config *LAFS) Downstream(b processor.Builder) {
	b.Add("fw0", &echo.Downlink{})
	if config.integritycheck {
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("erasure", reedsolomon.NewDownlink(
		config.datablocks,
		config.parityblocks,
		config.blocksize,
	))
	if config.encrypt {
		b.Add("encrypter", aes.NewDownlink(0, config.passphrase))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "erasure", "In")
	} else {
		b.Connect("fw0", "Out", "erasure", "In")
	}

	var dataechos int
	for i := 0; i < config.datablocks; i++ {
		echoID := fmt.Sprintf("echo%v", i)
		b.Add(echoID, &echo.Downlink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.DataStream)
		dataechos = i
	}

	for i := 0; i < config.parityblocks; i++ {
		echoID := fmt.Sprintf("echo%v", i+dataechos+1)
		b.Add(echoID, &echo.Downlink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.ParityStream)
	}
}
