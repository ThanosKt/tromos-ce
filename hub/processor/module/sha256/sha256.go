/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package sha256 // import "gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"

import (
	"crypto/sha256"
	"fmt"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func NewUplink() *Uplink {
	return &Uplink{}
}

func (up *Uplink) OnIn(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(in.Data, pw)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
	}

	in.Meta.State["checksum"] = fmt.Sprintf("%x", h.Sum(nil))
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(pr, in.Data)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
		return
	}

	if fmt.Sprintf("%x", h.Sum(nil)) != in.Meta.State["checksum"] {
		if err := in.Data.CloseWithError(processor.ErrCorrupted); err != nil {
			panic(err)
		}
		return
	}
}
