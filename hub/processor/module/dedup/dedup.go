/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package dedup // import "gitlab.com/tromos/tromos-ce/hub/processor/module/dedup"

import (
	"github.com/klauspost/dedup"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (up *Uplink) OnIn(in *processor.Stream) {
	pr, pw := processor.Pipe()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	// Create a new writer, with each block being 1000 bytes,
	// And allow it to use 10000 bytes of memory
	w, err := dedup.NewStreamWriter(pw, dedup.ModeFixed, 1000, 10000)
	if err != nil {
		panic(err)
	}
	defer w.Close()

	_, err = io.Copy(w, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	// Create a new stream reader:
	r, err := dedup.NewStreamReader(pr)
	if err != nil {
		panic(err)
	}
	defer r.Close()

	_, err = io.Copy(in.Data, r)
	if err != nil && err != io.EOF {
		panic(err)
	}
}
