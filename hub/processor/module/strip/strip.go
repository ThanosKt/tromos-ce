/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package strip // import "gitlab.com/tromos/tromos-ce/hub/processor/module/strip"

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In         <-chan *processor.Stream
	Out        []chan<- *processor.Stream
	stripesize int64
}

func NewUplink(stripesize int64) *Uplink {

	if stripesize == 0 {
		panic("Invalid stripe size")
	}
	up := &Uplink{stripesize: stripesize}
	return up
}

func (up *Uplink) OnIn(in *processor.Stream) {
	fanout := len(up.Out)
	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()
		defer pw[i].Close()

		up.Out[i] <- &processor.Stream{
			Data: pr[i],
			Meta: in.Meta,
		}
	}

	// Round-robin write to stripes
	var s int
	for i := 0; ; i++ {
		s = i % fanout
		_, err := io.CopyN(pw[s], in.Data, up.stripesize)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

type Downlink struct {
	processor.Module
	In         <-chan *processor.Stream
	Out        []chan<- *processor.Stream
	stripesize int64
}

func NewDownlink(stripesize int64) *Downlink {
	down := &Downlink{stripesize: stripesize}
	return down
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	fanout := len(down.Out)
	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()

		down.Out[i] <- &processor.Stream{
			Data: pw[i],
			Meta: in.Meta,
		}
	}

	// Round-robin read of the buffers to reconstruct the image
	// Return EOF only if data from all streams are consumed
	var s int
	isClosed := make([]bool, fanout)
	for i, done := 0, 0; done < fanout; i++ {
		s = i % fanout
		if !isClosed[s] {
			continue
		}
		_, err := io.CopyN(in.Data, pr[s], down.stripesize)
		if err == io.EOF {
			pr[s].Close()
			isClosed[s] = true
			done++
			continue
		}
		if err != nil {
			panic(err)
		}
	}
}
