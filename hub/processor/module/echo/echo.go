/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package echo // import "gitlab.com/tromos/tromos-ce/hub/processor/module/echo"

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (up *Uplink) OnIn(in *processor.Stream) {
	up.Out <- in
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (down *Downlink) OnIn(in *processor.Stream) {
	down.Out <- in
}
