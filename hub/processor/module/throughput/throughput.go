/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package throughput // import "gitlab.com/tromos/tromos-ce/hub/processor/module/throughput"

import (
	"fmt"
	"github.com/aybabtme/iocontrol"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In     <-chan *processor.Stream
	Out    chan<- *processor.Stream
	writer *iocontrol.MeasuredWriter
}

func (up *Uplink) Finish() {
	if up.writer == nil {
		return
	}
	fmt.Println("Uptream bytes per sec: ", up.writer.BytesPerSec())
}

func (up *Uplink) OnIn(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	up.writer = iocontrol.NewMeasuredWriter(pw)

	_, err := io.Copy(up.writer, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In     <-chan *processor.Stream
	Out    chan<- *processor.Stream
	reader *iocontrol.MeasuredReader
}

func (down *Downlink) Finish() {
	if down.reader == nil {
		return
	}
	fmt.Println("Downstream bytes per sec: ", down.reader.BytesPerSec())
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	down.reader = iocontrol.NewMeasuredReader(pr)

	_, err := io.Copy(in.Data, down.reader)
	if err != nil {
		panic(err)
	}
}
