/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package mirror // import "gitlab.com/tromos/tromos-ce/hub/processor/module/mirror"

import (
	"github.com/djherbis/stream"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out []chan<- *processor.Stream
}

func NewUplink() *Uplink {
	return &Uplink{}
}

func (up *Uplink) OnIn(in *processor.Stream) {

	w, err := stream.NewStream("", stream.NewMemFS())
	if err != nil {
		panic(err)
	}

	go func() {
		if _, err := io.Copy(w, in.Data); err != nil {
			panic(err)
		}
		w.Close()
	}()

	for i := 0; i < len(up.Out); i++ {
		r, err := w.NextReader()
		if err != nil {
			panic(err)
		}
		defer r.Close()

		pr, pw := processor.Pipe()
		up.Out[i] <- &processor.Stream{
			Data: pr,
			Meta: in.Meta,
		}

		if _, err := io.Copy(pw, r); err != nil {
			panic(err)
		}

		if err := pw.Close(); err != nil {
			panic(err)
		}
	}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out []chan<- *processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

func (down *Downlink) OnIn(in *processor.Stream) {
	// Always read from the first replicate
	// FIXME: add selection algorithm (probably another module)
	down.Out[0] <- in

	for i := 1; i < len(down.Out); i++ {
		down.Out[i] <- nil
	}
}
