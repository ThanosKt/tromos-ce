/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package aes // import "gitlab.com/tromos/tromos-ce/hub/processor/module/aes"

import (
	"crypto/aes"
	"crypto/cipher"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream

	mode int
	key  string
}

func NewUplink(mode int, key string) *Uplink {
	return &Uplink{
		mode: mode,
		key:  key,
	}
}

func (up *Uplink) OnIn(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()
	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(up.key))
	if err != nil {
		panic(err)
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])
	encrypter := &cipher.StreamWriter{S: stream, W: pw}
	defer encrypter.Close()

	_, err = io.Copy(encrypter, in.Data)
	if err != nil {
		panic(err)
	}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream

	mode int
	key  string
}

func NewDownlink(mode int, key string) *Downlink {

	return &Downlink{
		mode: mode,
		key:  key,
	}
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(down.key))
	if err != nil {
		panic(err)
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])

	decrypter := &cipher.StreamReader{S: stream, R: pr}

	if _, err := io.Copy(in.Data, decrypter); err != nil {
		panic(err)
	}
}
