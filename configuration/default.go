/*
* Copyright Fotis Nikolaidis (2019)
* Contributors :
* 	Fotis NIKOLAIDIS	nikolaidis.fotis@gmail.com
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA
*
* ---------------------------------------
 */

package configuration // import "gitlab.com/tromos/tromos-ce/configuration"

import (
	"github.com/spf13/viper"
	"time"
)

// Default Runtime Tromos Configuration
var Env *viper.Viper = viper.GetViper()

var (
	// This number is related to the pool. In general, you can associate with the number
	// of concurrently running files. If the number is set too low you may experience
	// data corruption when multiple files are concurrently running
	MaxConcurrentChannels = 20
)

func init() {

	// Used in HUB
	Env.SetDefault("TromosRoot", "/home/mochlos/GO/tromos/tromos-ce")
	Env.SetDefault("TromosHub", "/tmp/tromos-hub/")
	Env.SetDefault("GetBinaryPluginTimeout", 500*time.Second)
}
