== Tromos running on a single node

At this point, you should have tromos-cli and tromos-daemon successfully installed into your system. Now, it is time to start building your Manifest that describes the
environment of your virtual storage system (a.k.a storage container). In the folder $GOPATH/examples you will several existing templates of storage containers that you can adapt for your case. But, let's start from scratch.

    touch ./examples/mytutorial.yml

As we said earlier, the goal of this tutorial is to create a least-authority filestore that is using several platforms on its backend. Thereby, we have to create
several Virtual Device, each corresponding to a different backend. Let's start with a straightforward model with two Devices and one Coordinator.

[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/device/filesystem
            family: os
            path: /tmp/scratch/hdd0

    "dev1":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/device/googledrive
            credentials: ./docs/tutorial/credentials/account0.json
            
Coordinators:
    "coord0":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/coordinator/boltdb
            path: /tmp/databases/gzone
----

As you can see, the syntax for the Devices and Coordinators are similar. The `Persistent` layer describes the connector to the external data-stores where our data (for Devices) and metadata (for Coordinators) will be persistently stored. The only mandatory field is the `plugin`, whereas all the other fields are arguments to the plugin - Tromos is oblivious to the plugins and passes the arguments directly to the plugin constructor.
To find the arguments for its plugin, please consult the  link:https://gitlab.com/tromos/tromos-ce/hub/README.adoc[plugin documentation]. 


The first Device (dev0) is using as a backend a directory (/tmp/scratch/hdd0) in a locally mounted filesystem. Beware that the Device describes only access to a single directory, not to the whole filesystem. This makes it possible to have several Devices with distinct characteristics running atop the same filesystem. You will understand it better when we start discussing our Translators. The second Device (dev1) is using a Googledrive account at its backend. Typically, to let Tromos access your Googledrive account, you must 
link:https://www.iperiusbackup.net/en/how-to-enable-google-drive-api-and-get-client-credentials/[download your credentials and grant access] to Tromos. For the purposes of the tutorial, we have
already created some dummy accounts for you to test. The credentials are found within this directory.


You can bootstrap and mount the storage container like any other filesystem by using the following commands
    
    mkdir /tmp/mytutorial
    tromos-cli gateway fuse --manifest ./examples/mytutorial.yml --mountpoint /tmp/mytutorial

To test it, open a new console and type `echo testme > /tmp/mytutorial`. By doing `ls` on the directory `/tmp/scratch/hdd0` we will see that a file of 7 bytes has been created in the backend
of dev0

    7 Jun 20 02:30 /tmp/tromos/data/hdd0/root/8638af3e-ef3f-45bd-97bb-4f279fff70cd.0

Let's try something larger. For demo purpose, we are going to create a file of 10MB using the dd command.

    dd if=/dev/urandom of=/tmp/mytutorial/test bs=1MB count=10

By checking the `/tmp/scratch/hdd0`, you will see tons of small files being created.

    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.72
    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.73
    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.74
    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.75
    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.76
    128K Jun 20 02:32 /tmp/tromos/data/hdd0/root/dbaf744f-53fc-4887-a45d-f733008113cb.77


The reason is that Fuse is breaking the application-request (10MB) to several system-requests of 128K. Although Tromos can reconstruct the original from all these small system-requests,
it is not advisable to use it. Firstly because these small requests cause a significant load to the backend, and secondly because they pollute the Namespace. And thirdly, because we miss all the advantage of contiguous reading. The way to solve it is by using the `Translator` layers. The Translator layer precedes in the Device stack and handles the requests before
reaching the `Persistent` layer. Through that, we achieve several optimizations and customizations. For instance, we can equip devices with a `Burst-buffer` translator so to aggregate small-sized requests into large blobs before flushing them to the backend. Similarly, for coordinators, by controlling the way requests access the backend we can provide consistent on-demand.

The next snippet shows such an example. Note in `dev1` that the *translators are stackable*.


[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/device/filesystem
            family: os
            path: /tmp/scratch/hdd0

    "dev1":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/device/googledrive
            credentials: ./docs/tutorial/credentials/account0.json
            
        Translators:
            "0":
                plugin: gitlab.com/tromos/tromos-ce/hub/device/blob
                blocksize: 2M
            "1":
                plugin: gitlab.com/tromos/tromos-ce/hub/device/throttler
                rate: 500MB
                capacity: 1B
                regulate: channel
            
Coordinators:
    "coord0":
        Persistent:
            plugin: gitlab.com/tromos/tromos-ce/hub/coordinator/boltdb
            path: /tmp/databases/gzone
        Translators:
            "0":
                plugin: gitlab.com/tromos/tromos-ce/hub/coordinator/sequencer
                blockw2r: true
                blockw2w: true            
----


Now, if we try to run the previous `dd` commands again we will get 

    2.0M Jun 20 02:27 /tmp/tromos/data/hdd0/root/590d2204-7f7d-4d30-8dba-314810ee4ca3.0
    2.0M Jun 20 02:27 /tmp/tromos/data/hdd0/root/590d2204-7f7d-4d30-8dba-314810ee4ca3.1
    2.0M Jun 20 02:27 /tmp/tromos/data/hdd0/root/590d2204-7f7d-4d30-8dba-314810ee4ca3.2
    1.6M Jun 20 02:27 /tmp/tromos/data/hdd0/root/590d2204-7f7d-4d30-8dba-314810ee4ca3.3

The tons of previously small files have not been aggregated into larger blobs. 




== Processors 

As describes so far, the Devices are a framework for mapping custom data management layers into a single entity. This way, we can process the user's data before they
are sent to the backend for permanent storage. However, these management layers are specific to the Device. What happens if we want to distribute our data across several
different Devices? For this reason, we introduce the Processor.

What makes the Processor unique is that abstracts any datapath into a directed acyclic graph of processing components. For example, the next snippet will use the Processor designed for the
datapath of a least-authority filestore. If not specified otherwise, Tromos is using the "direct" graph by default.

Although the discussion of building your own I/O Processor is left for another tutorial, you can always have a look at the definitions of existing processors.
For example, you can find the definition of LAFS Processor on the link link:https://gitlab.com/tromos/tromos-ce/hub/processor/lafs/lib/lafs.go[]

*NOTE* The path of the LAFS definition and the path used on the snippet bellow are different. This is because the snippet includes the name of the module.


[source, yaml]
----
Processors:
    "proc0":
        Graph:  
            plugin: gitlab.com/tromos/tromos-ce/hub/processor/lafs
            integritycheck: true
            encrypt: true
            passphrase: 0123456789012345
            datablocks: 2
            parityblocks: 1
            blocksize: 1M

----




== Middleware

But ... wait again! How is the client going to know which Device to use? Similarly to how Device section describes the layers for a device, the Middleware section describes all the layers of the client middleware. 
Under the hood, Tromos set the configuration to the default following default. The DeviceManager chooses one of the available devices using a random selector, and the Namespace manager chooses the available coordinators using the consistent-hash selector. It must be noted that *the selector plugin for the Namespace manager must be deterministic* - for a given filename it should
always return the same Coordinator. Otherwise, it is impossible to find the right Coordinator to retrieve metadata from. Such deterministic selectors are the 
link:https://gitlab.com/tromos/tromos-ce/hub/selector/consistenthash/README.adoc[consistenthash] or the link:https://gitlab.com/tromos/tromos-ce/hub/selector/radix/README.adoc[radix].
Oppositely, the DeviceManager should distribute the load among several Device. Hence, selector plugins such as link:https://gitlab.com/tromos/tromos-ce/hub/selector/random/README.adoc[random] or
link:https://gitlab.com/tromos/tromos-ce/hub/selector/roundrobin/README.adoc[roundrobin] are preferred.


[source, yaml]
----
Middleware:
    DeviceManager:
        plugin: random 
    Namespace:
        plugin: consistenthash
----


For more advanced scenarios where we have Devices or Coordinators with distinct characteristics that we want to take advantage of them, we can use the 
link:https://gitlab.com/tromos/tromos-ce/hub/selector/byqos/README.adoc[byqos] selector. It parses the exposed capabilities of the Devices (or Coordinators) and
creates pools of similarly characterized resources. For more information link:https://gitlab.com/tromos/tromos-ce/blob/master/docs/tutorial/byqos.adoc[click here]